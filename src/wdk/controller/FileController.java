
package wdk.controller;

import java.io.IOException;
import properties_manager.PropertiesManager;
import static wdk.WDK_PropertyType.*;
import wdk.error.ErrorHandler;
import wdk.file.DraftFileManager;
import wdk.gui.MessageDialog;
import wdk.gui.WDK_GUI;
import wdk.gui.YesNoCancelDialog;
import wdk.WDK_PropertyType.*;

/**
 *This class handles responses based on interaction with the file toolbar
 * @author Sanjay
 */
public class FileController {
    boolean saved; 
    
    //READ AND WRITE TO A FILE 
    DraftFileManager draftIO;
    
    //DRAFT EXPORTER
    //DraftExporter draftExporter; 
    
    //USE THIS TO DISPLAY FEEDBACK
    MessageDialog messageDialog; 
    YesNoCancelDialog yesNoCancelDialog; 
    
    //PROPERTIES MANAGER 
    PropertiesManager properties; 

    //Now the Constructor 
    public FileController(MessageDialog initMessageDialog, 
            YesNoCancelDialog initYesNoCancelDialog,
            DraftFileManager initDraftIO){
        
        saved = true; 
        messageDialog = initMessageDialog; 
        yesNoCancelDialog = initYesNoCancelDialog; 
        draftIO = initDraftIO; 
        properties = PropertiesManager.getPropertiesManager();
    }
    
    /**
     * This Method will update the tool bar to alow the user to update or save 
     * a draft
     * @param gui will be the interface we will change 
     */
    public void markedAsEdited(WDK_GUI gui){
        //The file has been changed
        saved = false; 
        
        //let the gui know it can be saved. 
        gui.updateToolbarControls(saved); 
        
    }
    
    ////////////////////////
    // THE HANDLE METHODS //
    ////////////////////////
    /**
     * This method activated when the new draft button is on is clicked
     * 
     */
    public void handleNewDraftRequest(WDK_GUI gui){
        try{
            boolean continueToMakeNew = true;
            if(!saved){
                // THE USER CAN OPT OUT HERE WITH A CANCEL
                continueToMakeNew = promptToSave(gui);
            }
            
            if(continueToMakeNew){
                //RESET DATA
                
                //UPDATE CONTROLS
                saved = false; 
                gui.updateToolbarControls(saved);
                // TELL THE USER THE COURSE HAS BEEN CREATED
                messageDialog.show(properties.getProperty(NEW_DRAFT_CREATED_MESSAGE));
                
            }
        }catch(IOException e){
            e.printStackTrace();;
        }
    }
    
    /**
     * This method will exit the application, making sure the user doesn't lose
     * any data first.
     * 
     * @param gui
     */
    public void handleExitRequest(WDK_GUI gui) {
        try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToExit = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE
                continueToExit = promptToSave(gui);
            }

            // IF THE USER REALLY WANTS TO EXIT THE APP
            if (continueToExit) {
                // EXIT THE APPLICATION
                System.exit(0);
            }
        } catch (IOException ioe) {
            ErrorHandler eH = ErrorHandler.getErrorHandler();
            eH.handleExitError();
        }
    }
    
    private boolean promptToSave(WDK_GUI gui) throws IOException{
         yesNoCancelDialog.show(properties.getProperty(SAVE_UNSAVED_WORK_MESSAGE));
         //retrive the users slection
         String selection = yesNoCancelDialog.getSelection(); 
         //If yes save the draft
         if(selection.equals(YesNoCancelDialog.YES)){
             System.out.println("WILL IMPLEMENT THE SAVIGN LATER");
         }
         else if (selection.equals(YesNoCancelDialog.NO)){
             return false; 
         }
         
         return true; 
    }
    
    public void handleSaveRequest(WDK_GUI gui){
        try{
            draftIO.saveDraft(gui);
            // MARK IT AS SAVED
            saved = true;

            // TELL THE USER THE FILE HAS BEEN SAVED
            messageDialog.show(properties.getProperty(DRAFT_SAVED_MESSAGE));

            // AND REFRESH THE GUI, WHICH WILL ENABLE AND DISABLE
            // THE APPROPRIATE CONTROLS
            gui.updateToolbarControls(saved);
            
        }catch(IOException e){
            messageDialog.show("Failed to save"); 
        }
    }
}
