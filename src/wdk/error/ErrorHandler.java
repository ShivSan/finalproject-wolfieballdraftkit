/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.error;

import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import static wdk.WDK_Startup_Constants.CLOSE_BUTTON_LABEL;
import wdk.gui.MessageDialog;

/**
 *
 * @author Sanjay
 */
public class ErrorHandler {
    // THIS CLASS USES A SINGLETON DESIGN PATTERN, WHICH IS CONVENIENT
    // BECAUSE IT NEEDS TO BE USED BY SO MANY OTHER CLASSES
    static ErrorHandler singleton;
    
    // WE'LL MAKE USE OF THIS DIALOG TO PROVIDE OUR MESSAGE FEEDBACK
    MessageDialog messageDialog;
    
    // THE PROPERTIES MANAGER WILL GIVE US THE TEXT TO DISPLAY
    PropertiesManager properties;
        
    private ErrorHandler() {
        // THIS HELPS US KEEP TRACK OF WHETHER WE NEED TO
        // CONSTRUCT THE SINGLETON OR NOT EACH TIME IT'S ACCESSED
        singleton = null;
        
        // WE ONLY NEED TO GET THE SINGLETON ONCE
        properties = PropertiesManager.getPropertiesManager();
    }
    
    public static ErrorHandler getErrorHandler(){
        if(singleton == null){
            singleton = new ErrorHandler();
        }
        return singleton; 
    }
    
    // INIT THE DIALOG WE WILL USE TO DISPLAY ERRORS
    public void initMessageDialog(Stage stage){
        messageDialog = new MessageDialog(stage,CLOSE_BUTTON_LABEL); 
    }
    
    public void handleExitError(){
    
    }
    
    public void handleAddPlayerError(){
        messageDialog.show("Please complete all fields");
    }
}
