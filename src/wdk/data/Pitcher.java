/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.data;

import java.text.DecimalFormat;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;

/**
 *
 * @author Sanjay
 */
public class Pitcher extends Player{
    
    private DoubleProperty IP; // Innings Pitched
    private IntegerProperty ER; // Earned Runs
    private IntegerProperty W; // Wins
    private IntegerProperty SV; // Saves
    private IntegerProperty K; // Strike otus
    private IntegerProperty H; // Hits
    private IntegerProperty BB; // Walks
    private DoubleProperty ERA; // Earned run average
    private DoubleProperty WHIP; //With runners in position
    
    // FOR FORMATING DECIMALS 
    DecimalFormat df = new DecimalFormat("#.###");

    public Pitcher(){
        IP = new SimpleDoubleProperty(0); 
        ER = new SimpleIntegerProperty(0);
        W = new SimpleIntegerProperty(0);
        SV = new SimpleIntegerProperty(0);
        K = new SimpleIntegerProperty(0);
        H = new SimpleIntegerProperty(0);
        BB = new SimpleIntegerProperty(0);
        ERA = new SimpleDoubleProperty(0);
        WHIP = new SimpleDoubleProperty(0);
    }
//Getters and setters
    
    public double getIP(){
        return this.IP.get();
    }
    
    public void setIP(double IP){
        this.IP.set(IP);
    }
    
    public int getER(){
        return this.ER.get(); 
    }
    
    public void setER(int ER){
        this.ER.set(ER);
    }
    
    public int getW(){
        return this.W.get(); 
    }
    
    public void setW(int W){
        this.W.set(W); 
    }
    
    public int getSV(){
        return this.SV.get(); 
    }
    
    public void setSV(int SV){
        this.SV.set(SV);
    }
    
    public int getK(){
        return this.K.get(); 
    }
    
    public void setK(int K){
        this.K.set(K);
    }
    
    public int getBB(){
        return this.BB.get(); 
    }
    
    public void setBB(int BB){
        this.BB.set(BB);
    }
    
    public int getH(){
        return this.H.get(); 
    }
    
    public void setH(int H){
        this.H.set(H);
    }
    
    public double getERA(){
        return ERA.get(); 
    }
    
    public void setERA(double ERA){
        this.ERA.set(ERA);
    }
    
    public double getWHIP(){
        return this.WHIP.get(); 
    }
    
    public void setWHIP(double WHIP){
        this.WHIP.set(WHIP);
    }
    
}