/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.data;

import java.text.DecimalFormat;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Sanjay
 */
public class Hitter extends Player{
    private IntegerProperty AB; //At Bats
    private IntegerProperty R;// Runs
    private IntegerProperty H; //Hits
    private IntegerProperty HR;// HomeRuns
    private IntegerProperty RBI;//Runs Batted In
    private IntegerProperty SB; //Stolen Bases
    private DoubleProperty BA; //Batting Avergae
    
    //FOR FORMATING DECIMALS 
    DecimalFormat df = new DecimalFormat("#.###");
    
    public Hitter(){
        AB = new SimpleIntegerProperty(0);
        R = new SimpleIntegerProperty(0);
        H = new SimpleIntegerProperty(0);
        HR = new SimpleIntegerProperty(0);
        RBI = new SimpleIntegerProperty(0);
        SB = new SimpleIntegerProperty(0);
        BA = new SimpleDoubleProperty(0);
        
    }
    
    public Hitter(StringProperty lastName, StringProperty firstName, StringProperty team, 
            IntegerProperty YEAR_OF_BIRTH, String notes, StringProperty NATION_OF_BIRTH){
        
        super(lastName, firstName,team,YEAR_OF_BIRTH, notes, NATION_OF_BIRTH);
        
    }

    public int getAB() {
        return AB.get();
    }

    public void setAB(int AB) {
        this.AB.set(AB);
    }

    public int getR() {
        return R.get();
    }

    public void setR(int R) {
        this.R.set(R);
    }

    public int getH() {
        return H.get();
    }

    public void setH(int H) {
        this.H.set(H);
    }

    public int getHR() {
        return HR.get();
    }

    public void setHR(int HR) {
        this.HR.set(HR);
    }

    public int getRBI() {
        return RBI.get();
    }

    public void setRBI(int RBI) {
        this.RBI.set(RBI);
    }

    public int getSB() {
        return SB.get();
    }

    public void setSB(int SB) {
        this.SB.set(SB);
    }

    public double getBA() {
        return Double.parseDouble(df.format(BA.get()));
    }

    public void setBA(double BA) {
        this.BA.set(BA);
    }
    
}
