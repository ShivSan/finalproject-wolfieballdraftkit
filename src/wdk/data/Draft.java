/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.data;

import java.util.ArrayList;

/**
 * This is the main structure that is being edited when using the program
 * @author Sanjay
 */
public class Draft {
    private String name; 
    private ArrayList<FantasyTeam> teams; 

    public Draft(){
        this.name = ""; 
        teams = new ArrayList(); 
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<FantasyTeam> getTeams() {
        return teams;
    }

    public void setTeams(ArrayList<FantasyTeam> teams) {
        this.teams = teams;
    }
}
