/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.data;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * The main class to form objects of the players
 * Extends into hitters and pitchers
 * @author Sanjay
 */
public class Player {
    private StringProperty lastName;
    private StringProperty firstName; 
    private StringProperty team; 
    private IntegerProperty YEAR_OF_BIRTH; 
    private String notes; 
    private StringProperty NATION_OF_BIRTH; 
    private StringProperty QP; 
    private StringProperty contract; 
    private FantasyTeam fTeam; 
    private int salary; 
    private boolean drafted; 
    private StringProperty fQP; 

    public Player(){
        this.lastName = new SimpleStringProperty("");
        this.firstName = new SimpleStringProperty(""); 
        this.team = new SimpleStringProperty("");
        this.NATION_OF_BIRTH = new SimpleStringProperty("");
        this.YEAR_OF_BIRTH = new SimpleIntegerProperty(1);
        this.QP = new SimpleStringProperty(""); 
        this.contract = new SimpleStringProperty("");
        this.fQP = new SimpleStringProperty(""); 
    }
    
    public Player(StringProperty lastName, StringProperty firstName, StringProperty team, 
            IntegerProperty YEAR_OF_BIRTH, String notes, StringProperty NATION_OF_BIRTH) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.team = team;
        this.YEAR_OF_BIRTH = YEAR_OF_BIRTH;
        this.notes = notes;
        this.NATION_OF_BIRTH = NATION_OF_BIRTH;
    }

    public String getLastName() {
        return lastName.get();
    }

    public void setLastName(String lastName) {
        this.lastName.set(lastName);
    }

    public String getFirstName() {
        return firstName.get();
    }

    public void setFirstName(String firstName) {
        this.firstName.set(firstName);
    }

    public String getTeam() {
        return team.get();
    }

    public void setTeam(String team) {
        this.team.set(team);
    }

    public int getYEAR_OF_BIRTH() {
        return YEAR_OF_BIRTH.get();
    }

    public void setYEAR_OF_BIRTH(int YEAR_OF_BIRTH) {
        this.YEAR_OF_BIRTH.set(YEAR_OF_BIRTH);
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getNATION_OF_BIRTH() {
        return NATION_OF_BIRTH.get();
    }

    public void setNATION_OF_BIRTH(String NATION_OF_BIRTH) {
        this.NATION_OF_BIRTH.set(NATION_OF_BIRTH);
    }
    
    public String getQP() {
        return QP.get();
    }
    
    public void setQP(String QP) {
        this.QP.set(QP);
    }
    public String getFQP() {
        return fQP.get();
    }
    
    public void setFQP(String fQP) {
        this.fQP.set(fQP);
    }
    public String getContract() {
        return contract.get();
    }
    
    public void setContract(String contract) {
        this.contract.set(contract);
    }

    public FantasyTeam getfTeam() {
        return fTeam;
    }

    public void setFantasyTeam(FantasyTeam fTeam) {
        this.fTeam = fTeam;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public boolean isDrafted() {
        return drafted;
    }

    public void setDrafted(boolean drafted) {
        this.drafted = drafted;
    }
    
}
