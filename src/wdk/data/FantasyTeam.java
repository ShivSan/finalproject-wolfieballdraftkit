/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.data;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import static javax.management.Query.value;
import static wdk.WDK_Startup_Constants.*;

/**
 * THis is the structure for a fantasy team 
 * @author Sanjay
 */
public class FantasyTeam {
    private String name;
    private String owner; 
    private ObservableList<Player> roster; 
    private ObservableList<Player> taxiSquad; 
    private int payroll; 
    private HashMap<String, Integer> rosterSpots; //POS/amount filled; 
    private double ERA; 
    private int H; 
    private int R; 
    private int W;
    private int RBI; 
    private int SB; 
    private int HR;
    private double BA; 
    private double WHIP; 
    private int SV; 
    private int K; 
    private int points; 

   public FantasyTeam(){
       roster = FXCollections.observableList(new ArrayList()); 
       taxiSquad  = FXCollections.observableList(new ArrayList());
       rosterSpots = new HashMap(); 
       payroll = 260; 
       initMap(); 
   }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ObservableList<Player> getRoster() {
        return roster;
    }

    public void setRoster(ObservableList<Player> roster) {
        this.roster = roster;
    }

    public int getPayroll() {
        return payroll;
    }

    public void setPayroll(int payroll) {
        this.payroll = payroll;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public double getERA() {
        try{
        BigDecimal bigDecimal = new BigDecimal(this.ERA);
        bigDecimal = bigDecimal.setScale(2,
        BigDecimal.ROUND_HALF_UP);
        return bigDecimal.doubleValue();
        }catch(Exception e){
            return 0; 
        } 
    }

    public void setERA(double ERA) {
        this.ERA = ERA;
    }

    public int getH() {
        return H;
    }

    public void setH(int H) {
        this.H = H;
    }

    public int getR() {
        return R;
    }

    public void setR(int R) {
        this.R = R;
    }

    public int getW() {
        return W;
    }

    public void setW(int W) {
        this.W = W;
    }

    public int getRBI() {
        return RBI;
    }

    public void setRBI(int RBI) {
        this.RBI = RBI;
    }

    public int getSB() {
        return SB;
    }

    public void setSB(int SB) {
        this.SB = SB;
    }

    public double getBA() {
        try{
        BigDecimal bigDecimal = new BigDecimal(this.BA);
        bigDecimal = bigDecimal.setScale(3,
        BigDecimal.ROUND_HALF_UP);
        return bigDecimal.doubleValue();
        }catch(Exception e){
            return .000; 
        }
    }

    public void setBA(double BA) {
        this.BA = BA;
    }

    public double getWHIP() {
        try{
        BigDecimal bigDecimal = new BigDecimal(this.WHIP);
        bigDecimal = bigDecimal.setScale(2,
        BigDecimal.ROUND_HALF_UP);
        return bigDecimal.doubleValue();
        }catch(Exception e){
            return 0.00; 
        }
    }

    public void setWHIP(double WHIP) {
        this.WHIP = WHIP;
    }

    public int getSV() {
        return SV;
    }

    public void setSV(int SV) {
        this.SV = SV;
    }

    public int getK() {
        return K;
    }

    public void setK(int K) {
        this.K = K;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }
    
    public void setHR(int HR){
        this.HR = HR; 
    }
    
    public int getHR(){
        return this.HR; 
    }

    
    @Override
    public String toString(){
        return this.name; 
    }

    public ObservableList<Player> getTaxiSquad() {
        return taxiSquad;
    }

    public void setTaxiSquad(ObservableList<Player> taxiSquad) {
        this.taxiSquad = taxiSquad;
    }

    public HashMap<String, Integer> getRosterSpots() {
        return rosterSpots;
    }

    public void setRosterSpots(HashMap<String, Integer> rosterSpots) {
        this.rosterSpots = rosterSpots;
    }
    
    public void initMap(){
        this.rosterSpots.put(POSITION_C,0); 
        this.rosterSpots.put(POSITION_1B,0);
        this.rosterSpots.put("CI",0);
        this.rosterSpots.put(POSITION_3B,0);
        this.rosterSpots.put(POSITION_2B,0);
        this.rosterSpots.put("MI",0);
        this.rosterSpots.put(POSITION_SS,0);
        this.rosterSpots.put(POSITION_OF,0); 
        this.rosterSpots.put("U",0);
        this.rosterSpots.put(POSITION_P,0); 
     
    }
    
    public boolean addPlayer(Player p,String pos){
        //CHECK THE AMOUNT OF PLAYERS FOR THAT ROLE
        int spots = rosterSpots.get(pos);
        //FOR CATCHERS
        if(pos.equals(POSITION_C)){
            //ONLY ALLOWED 2 CATCHERS   
            if(spots < 2){
                rosterSpots.put(pos,rosterSpots.get(pos) + 1);
                roster.add(p);
                payroll  = payroll - p.getSalary(); 
                calcStats(); 
                return true; 
             }
            else{
                System.out.println("TOO MANY CATCHERS");
                return false; 
            }
        }
        
        //FOR 1B
        if(pos.equals(POSITION_1B)){
            //ONLY ALLOWED 1 1B   
            if(spots < 1){
                rosterSpots.put(pos,rosterSpots.get(pos) + 1);
                roster.add(p);
                payroll  = payroll - p.getSalary(); 
                calcStats(); 
                return true; 
            }
            else{
                System.out.println("TOO MANY 1B");
                return false;
            }        
        }

        //FOR 2B
        if(pos.equals(POSITION_2B)){
            //ONLY ALLOWED 1 2B   
            if(spots < 1){
                rosterSpots.put(pos,rosterSpots.get(pos) + 1);
                roster.add(p);
                payroll  = payroll - p.getSalary(); 
                calcStats(); 
                return true; 
            }
            else{
                System.out.println("TOO MANY 2B");
                return false; 
            }        
        }
        
        //FOR 3B
        if(pos.equals(POSITION_3B)){
            //ONLY ALLOWED 1 3B   
            if(spots < 1){
                rosterSpots.put(pos,rosterSpots.get(pos) + 1);
                roster.add(p);
                payroll  = payroll - p.getSalary(); 
                calcStats(); 
                return true;
            }
            else{
                System.out.println("TOO MANY 3B");
                return false; 
            }        
        }

        //FOR SS
        if(pos.equals(POSITION_SS)){
            //ONLY ALLOWED 1 SS   
            if(spots < 1){
                rosterSpots.put(pos,rosterSpots.get(pos) + 1);
                roster.add(p);
                payroll  = payroll - p.getSalary(); 
                calcStats(); 
                return true; 
            }
            else{
                System.out.println("TOO MANY SS");
                return false; 
            }        
        }
        
        //FOR U
        if(pos.equals("U")){
            //ONLY ALLOWED 1 U   
            if(spots < 1){
                rosterSpots.put(pos,rosterSpots.get(pos) + 1);
                roster.add(p);
                payroll  = payroll - p.getSalary(); 
                calcStats(); 
                return true; 
            }
            else{
                System.out.println("TOO MANY U");
                return false;
            }        
        }
        
        //FOR CI
        if(pos.equals("CI")){
            //ONLY ALLOWED 1 CI   
            if(spots < 1){
                rosterSpots.put(pos,rosterSpots.get(pos) + 1);
                roster.add(p);
                payroll  = payroll - p.getSalary(); 
                calcStats(); 
                return true; 
            }
            else{
                System.out.println("TOO MANY CI");
                return false; 
            }        
        }
        
        //FOR MI
        if(pos.equals("MI")){
            //ONLY ALLOWED 1 U   
            if(spots < 1){
                rosterSpots.put(pos,rosterSpots.get(pos) + 1);
                roster.add(p);
                payroll  = payroll - p.getSalary(); 
                calcStats(); 
                return true; 
            }
            else{
                System.out.println("TOO MANY MI");
                return false; 
            }        
        }
        
        //FOR OF
        if(pos.equals(POSITION_OF)){
            //ONLY ALLOWED 5 OF   
            if(spots < 5){
                rosterSpots.put(pos,rosterSpots.get(pos) + 1);
                roster.add(p);
                payroll  = payroll - p.getSalary(); 
                calcStats(); 
                return true; 
            }
            else{
                System.out.println("TOO MANY OF");
                return false; 
            }        
        }
        if(pos.equals(POSITION_P)){
            //ONLY ALLOWED 9 pitchers   
            if(spots < 9){
                rosterSpots.put(pos,rosterSpots.get(pos) + 1);
                roster.add(p);
                payroll  = payroll - p.getSalary(); 
                calcStats(); 
                return true; 
            }
            else{
                System.out.println("TOO MANY P");
                return false;
            }        
        }
        return false; 
    }
    
    public void calcStats(){
        int r = 0;
        int h = 0;
        int ip = 0; 
        int ab = 0; 
        int w = 0; 
        int rbi = 0; 
        int sb = 0; 
        int sv = 0; 
        int k = 0; 
        int bb = 0; 
        int hits = 0; 
        int er = 0; 
        int hr = 0; 
        double era = 0; 
        double whip = 0; 
        double ba = 0; 
        for(Player p : roster){
            //IF HITTER
            try{
                Hitter hitter = ((Hitter)p); 
                h += hitter.getH();
                r += hitter.getR(); 
                rbi += hitter.getRBI(); 
                sb += hitter.getSB(); 
                ab += hitter.getAB();
                hr += hitter.getHR(); 
                rbi += hitter.getRBI(); 
            }catch(Exception e){}
            //IF PITCHER
            try{
                Pitcher pitcher = ((Pitcher)p); 
                ip += pitcher.getIP(); 
                w += pitcher.getW(); 
                sv += pitcher.getSV();
                k += pitcher.getK();
                hits = pitcher.getH(); 
                bb += pitcher.getBB(); 
                er += pitcher.getER(); 
            }catch(Exception e){}
        }
        this.BA = ((double)h/(double)ab);
        this.ERA = ((double)er/(double)ip) * 9;
        this.WHIP = ((double)bb +(double)hits)/(double)ip; 
        this.H = h;
        this.K = k; 
        this.W = w; 
        this.R = r; 
        this.SV = sv; 
        this.HR = hr;
        this.SB = sb; 
        this.RBI = rbi; 
    }
    public void removePlayer(Player p){
        String pos = p.getFQP(); 
        this.roster.remove(p); 
        this.rosterSpots.put(pos,(int)rosterSpots.get(pos) - 1);
    }
    
    public int calcMaxBet(){
        return payroll - this.getRemainingSpots(); 
    }
    
    public int getRemainingSpots(){
        int spots = 0; 
        for(int x : rosterSpots.values()){
            spots += x; 
        }
        
        return 23 - spots; 
    }
    
    public boolean isFull(){
        int x = this.getRemainingSpots(); 
        return x == 0; 
    }
    
    public int getDraftedHitters(){
        int hitters = 0;  
        for(Player p : roster){
            try{
                Hitter h = ((Hitter)p); 
                hitters++; 
            }catch(Exception e){}
        }
        return (hitters); 
    }
    
    public int getDraftedPitchers(){
        int pitchers = 0;  
        for(Player p : roster){
            try{
                Pitcher pt = ((Pitcher)p); 
                pitchers++; 
            }catch(Exception e){}
        }
        return (pitchers); 
    }
    
}
