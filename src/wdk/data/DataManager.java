/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.data;

import java.util.ArrayList;
import java.util.HashMap;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *This class contains program data that can be used an referenced such as the 
 * Library of players. 
 * @author Sanjay
 */
public class DataManager {
    private Draft draft; 
    private ArrayList<Player> hitters; 
    private ArrayList<Player> pitchers; 
    private ArrayList<Player> players;
    private ArrayList<Player> sortedHitters; 
    private ArrayList<Player> sortedPitchers;
    private ObservableList<Player> displayList; 
    private ObservableList<String> mlbTeams; 
    private ObservableList<Player> draftedPlayers; 
    
    private ObservableList<FantasyTeam> teams; 
    
    private ArrayList<String> contracts; 
    
    public DataManager(){
        hitters = new ArrayList(); 
        pitchers = new ArrayList(); 
        displayList = FXCollections.observableList(new ArrayList()); 
        mlbTeams = FXCollections.observableList(new ArrayList());
        players = new ArrayList(); 
        teams = FXCollections.observableList(new ArrayList()); 
        draftedPlayers = FXCollections.observableList(new ArrayList()); 
        draft = new Draft(); 
    }
    
    public ArrayList<Player> getHitters(){
        return this.hitters; 
    }
    
    public ArrayList<Player> getPicthers(){
        return this.pitchers; 
    }
    
    public ArrayList<Player> getSortedHitters() {
        return sortedHitters;
    }
    
    public void setSortedHitters(ArrayList<Player> list){
        this.sortedHitters = list; 
    }

    public ArrayList<Player> getPitchers() {
        return pitchers;
    }

    public void setPitchers(ArrayList<Player> pitchers) {
        this.pitchers = pitchers;
    }

    public ArrayList<Player> getSortedPitchers() {
        return sortedPitchers;
    }

    public void setSortedPitchers(ArrayList<Player> sortedPitchers) {
        this.sortedPitchers = sortedPitchers;
    }

    public ObservableList<Player> getDisplayList() {
        return displayList;
    }

    public void setDisplayList(ObservableList<Player> displayList) {
        this.displayList = displayList;
    }

    public ObservableList<String> getMlbTeams() {
        return mlbTeams;
    }

    public void setMlbTeams(ObservableList<String> mlbTeams) {
        this.mlbTeams = mlbTeams;
    }
    
    public void setPlayers(){
        
    }
    
    public ArrayList<Player> getPlayers(){
       return this.players; 
    }
    
    public ObservableList<FantasyTeam> getTeams(){
       return this.teams; 
    }

    public ArrayList<String> getContracts() {
        return contracts;
    }

    public void setContracts(ArrayList<String> contracts) {
        this.contracts = contracts;
    }
    //ADD AND REMOVE PLAYERS FROM THIER LISTS 
    public void addHitter(Hitter h){
        hitters.add(h); 
    }
    
    public void removeHitter(Hitter h){
        hitters.remove(h); 
    }
    
    public void addPitcher(Pitcher p){
        pitchers.add(p); 
    }
    
    public void addTeam(String team){
        mlbTeams.add(team); 
    }
    
    public void initDisplayList(){
        displayList = FXCollections.observableList(players);
    }
    public void createPlayers(){
        for(Player p : hitters){
            players.add(p); 
        }
        
        for(Player p : pitchers){
            players.add(p); 
        }
    }
    
    public void addPlayer(Player p ){
        players.add(p); 
    }
    
    public void addTeam(FantasyTeam team){
        teams.add(team); 
    }

    public ObservableList<Player> getDraftedPlayers() {
        return draftedPlayers;
    }

    public void setDraftedPlayers(ObservableList<Player> draftedPlayers) {
        this.draftedPlayers = draftedPlayers;
    }

    public Draft getDraft() {
        return draft;
    }

    public void setDraft(Draft draft) {
        this.draft = draft;
    }
}
