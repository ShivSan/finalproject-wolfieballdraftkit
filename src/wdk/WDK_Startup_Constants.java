/*
 * This class will contain constans such as filepaths for the WolfieBall Draft Kit application
 */
package wdk;

/**
 *
 * @author Sanjay
 */
public class WDK_Startup_Constants {
    //APP TITLE
    public static final String APP_TITLE = "Wolfie Ball Draft Kit"; 
    // WE NEED THESE CONSTANTS JUST TO GET STARTED
    // LOADING SETTINGS FROM OUR XML FILES
    public static final String PROPERTIES_FILE_NAME = "properties.xml";
    public static final String PROPERTIES_SCHEMA_FILE_NAME = "properties_schema.xsd";    
    public static final String PATH_DATA = "./data/";
    public static final String PATH_DRAFTS = PATH_DATA + "drafts/";
    public static final String PATH_IMAGES = "./images/";
    public static final String PATH_CSS = "wdk/css/";
    public static final String PATH_SITES = "sites/";
    public static final String PATH_BASE = PATH_SITES + "base/";
    public static final String PATH_EMPTY = ".";

    // THESE ARE THE DATA FILES WE WILL LOAD AT STARTUP
    public static final String JSON_FILE_PATH_HITTERS = PATH_DATA + "hitters.json";
    public static final String JSON_FILE_PATH_PITCHERS = PATH_DATA + "pitchers.json";
    public static final String JSON_FILE_PATH_CONTRACTS = PATH_DATA + "contracts.json";
    
    // ERRO MESSAGE ASSOCIATED WITH PROPERTIES FILE LOADING ERRORS
    public static String PROPERTIES_FILE_ERROR_MESSAGE = "Error Loading properties.xml";

    // ERROR DIALOG CONTROL
    public static String CLOSE_BUTTON_LABEL = "Close";
    
    //PLAYER POSITIONS 
    public static String POSITION_C = "C"; 
    public static String POSITION_1B = "1B"; 
    public static String POSITION_2B = "2B"; 
    public static String POSITION_SS = "SS"; 
    public static String POSITION_3B = "3B"; 
    public static String POSITION_CI = "CI"; 
    public static String POSITION_MI = "MI"; 
    public static String POSITION_OF = "OF"; 
    public static String POSITION_P = "P"; 
    
}
