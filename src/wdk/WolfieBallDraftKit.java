/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk;

import java.io.IOException;
import java.util.ArrayList;
import static wdk.WDK_Startup_Constants.*;
import static wdk.WDK_PropertyType.*;
import java.util.Locale;
import javafx.application.Application;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import wdk.data.DataManager;
import wdk.data.Player;
import wdk.file.JsonDraftFileManager;
import wdk.gui.WDK_GUI;
/**
 *
 * @author Sanjay
 */
public class WolfieBallDraftKit extends Application {
    // The only field this class has is the GUI
    WDK_GUI gui; 
    @Override
    public void start(Stage primaryStage) {
        
        boolean sucess = loadProperties();
        if(sucess){
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            String appTitle = props.getProperty(PROP_APP_TITLE);
            JsonDraftFileManager jsonManager = new JsonDraftFileManager();
           
            try{
                ArrayList<String> hitters =  jsonManager.loadHitters(JSON_FILE_PATH_HITTERS);
                ArrayList<String> pitchers = jsonManager.loadPitchers(JSON_FILE_PATH_PITCHERS); 
                gui = new WDK_GUI(primaryStage,jsonManager);    
                jsonManager.loadHitters(gui, JSON_FILE_PATH_HITTERS);
                jsonManager.loadPitchers(gui, JSON_FILE_PATH_PITCHERS);
                jsonManager.loadContracts(gui,JSON_FILE_PATH_CONTRACTS); 
                gui.getDataManager().setSortedHitters(gui.getDataManager().getHitters());
                gui.initGUI(props.getProperty(PROP_APP_TITLE), hitters, pitchers);
                
            }catch(IOException e){
                e.printStackTrace();
            }
        }
    }
    
    //Load the properties from the XML file
    public boolean loadProperties() {
        try {
            // LOAD THE SETTINGS FOR STARTING THE APP
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            props.addProperty(PropertiesManager.DATA_PATH_PROPERTY, PATH_DATA);
            props.loadProperties(PROPERTIES_FILE_NAME, PROPERTIES_SCHEMA_FILE_NAME);
            return true;
       } catch (Exception e) {
            // SOMETHING WENT WRONG INITIALIZING THE XML FILE
                //ErrorHandler eH = ErrorHandler.getErrorHandler();
                //eH.handlePropertiesFileError();
            e.printStackTrace();
            return false;
        }        
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Locale.setDefault(Locale.US);
        launch(args);
    }
    
}
