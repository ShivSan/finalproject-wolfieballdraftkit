/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk;

/**
 * This enum will hold the propertied loaded in from the XML files. 
 * @author Sanjay
 */
public enum WDK_PropertyType {
    //APP TITLE
    PROP_APP_TITLE,
    //BUTTON ICONS
    NEW_DRAFT_ICON,
    LOAD_DRAFT_ICON,
    SAVE_DRAFT_ICON,
    EXPORT_DRAFT_ICON,
    DELETE_ICON,
    EXIT_ICON,
    ADD_ICON,
    MINUS_ICON,
    MOVE_UP_ICON,
    MOVE_DOWN_ICON,
    FREE_AGENT_ICON,
    FANTASY_TEAMS_ICON,
    FANTASY_STANDINGS_ICON,
    DRAFT_SUMMARY_ICON,
    MLB_TEAMS_ICON,
    PLAY_BUTTON_ICON,
    PAUSE_BUTTON_ICON,
    
    //PAGE HEADINGS
    AVALIABLE_PLAYERS_HEADING,
    FANTASY_STANDINGS_HEADING,
    DRAFT_SUMMARY_HEADING,
    FANTASY_TEAMS_HEADING,
    MLB_TEAMS_HEADING,
    //WORKSPACE LABELS
    SEARCH_LABEL,
    
    //BUTTON TOOLTIPS
    NEW_DRAFT_TOOLTIP,
    SAVE_DRAFT_TOOLTIP,
    LOAD_DRAFT_TOOLTIP,
    EXPORT_DRAFT_TOOLTIP,
    EXIT_TOOLTIP,
    PLAYER_TOOLTIP,
    FANTASY_TEAMS_TOOLTIP,
    FANTASY_STANDINGS_TOOLTIP,
    MLB_TEAMS_TOOLTIP,
    DRAFT_SUMMARY_TOOLTIP,
    ADD_TEAM_TOOLTIP,
    DELETE_TEAM_TOOLTIP,
    EDIT_TEAM_TOOLTIP,
    FREE_PLAYER_TOOLTIP,
    START_DRAFT_TOOLTIP,
    PAUSE_DRAFT_TOOLTIP,
    ADD_RANDOM_PLAYER_TOOLTIP,
    
    //MESSAGES
    NEW_DRAFT_CREATED_MESSAGE,
    DRAFT_SAVED_MESSAGE,
    SAVE_UNSAVED_WORK_MESSAGE
    
}
