/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.file;

import java.io.IOException;
import java.util.ArrayList;
import wdk.gui.WDK_GUI;

/**
 *
 * @author Sanjay
 */
public interface DraftFileManager {
   public ArrayList<String> loadHitters(String filePath)throws IOException;
   public ArrayList<String> loadPitchers(String filePath)throws IOException;
   public void saveDraft(WDK_GUI gui) throws IOException; 
}
