/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.file;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import javafx.collections.ObservableList;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.json.JsonWriter;
import static wdk.WDK_Startup_Constants.PATH_DRAFTS;
import wdk.data.DataManager;
import wdk.data.Draft;
import wdk.data.FantasyTeam;
import wdk.data.Hitter;
import wdk.data.Pitcher;
import wdk.data.Player;
import wdk.gui.WDK_GUI;

/**
 *
 * @author Sanjay
 */
public class JsonDraftFileManager implements DraftFileManager{
    //Constants to help us load and set data
    String JSON_DRAFTS = "drafts";
    String JSON_DRAFT = "draft";
    String JSON_HITTERS = "Hitters";
    String JSON_PITCHERS = "Pitchers"; 
    String JSON_LASTNAME = "LAST_NAME";
    String JSON_FIRSTNAME = "FIRST_NAME"; 
    String JSON_TEAM = "TEAM";
    String JSON_QP = "QP";
    String JSON_AB = "AB"; 
    String JSON_R = "R";
    String JSON_H = "H";
    String JSON_HR = "HR";
    String JSON_RBI = "RBI";
    String JSON_SB = "SB";
    String JSON_IP = "IP"; 
    String JSON_W = "W"; 
    String JSON_ER = "ER"; 
    String JSON_SV = "SV";
    String JSON_BB = "BB";
    String JSON_K = "K"; 
    String JSON_NOTES = "NOTES"; 
    String JSON_YEAR_OF_BIRTH = "YEAR_OF_BIRTH";
    String JSON_NATION_OF_BIRTH = "NATION_OF_BIRTH"; 
    String JSON_CONTRACTS = "contracts"; 
    String JSON_CONTRACT = "contract";
    String JSON_EXT = ".json"; 
    String JSON_FREE_AGENTS = "free agents"; 
    String JSON_TEAM_NAME = "name"; 
    String JSON_ROSTER = "roster"; 
    String JSON_OWNER = "owner"; 
    String JSON_TEAMS = "teams"; 
    String JSON_DRAFTED_PLAYERS = "draftedPlayers"; 
    String JSON_FQP = "fQP";     
    String JSON_SALARY = "salary"; 
    
    // LOADS A JSON FILE AS A SINGLE OBJECT AND RETURNS IT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;
    }
    // LOADS AN ARRAY OF A SPECIFIC NAME FROM A JSON FILE AND
    // RETURNS IT AS AN ArrayList FULL OF THE DATA FOUND
    private ArrayList<String> loadArrayFromJSONFile(String jsonFilePath, String arrayName) throws IOException {
        JsonObject json = loadJSONFile(jsonFilePath);
        ArrayList<String> items = new ArrayList();
        JsonArray jsonArray = json.getJsonArray(arrayName);
        for (JsonValue jsV : jsonArray) {
            items.add(jsV.toString());
        }
        return items;
    }
    
    @Override
    public ArrayList<String> loadHitters(String filePath)throws IOException{
        ArrayList<String> hittersArray = loadArrayFromJSONFile(filePath,JSON_HITTERS);
        ArrayList<String> cleanedArray = new ArrayList();
        for (String s : hittersArray) {
            // GET RID OF ALL THE QUOTE CHARACTERS
            s = s.replaceAll("\"", "");
            cleanedArray.add(s);
        }
        return cleanedArray;
    }
    
    @Override
    public ArrayList<String> loadPitchers(String filePath)throws IOException{
        ArrayList<String> pitchersArray = loadArrayFromJSONFile(filePath,JSON_PITCHERS);
        ArrayList<String> cleanedArray = new ArrayList();
        for (String s : pitchersArray) {
            // GET RID OF ALL THE QUOTE CHARACTERS
            s = s.replaceAll("\"", "");
            cleanedArray.add(s);
        }
        return cleanedArray;
    }
    
    public void loadHitters(WDK_GUI gui, String jsonFilepath)throws IOException{
        DataManager dataManager = gui.getDataManager(); 
        JsonObject json = loadJSONFile(jsonFilepath); 
        
        //LOAD THE HITTERS ARRAY 
        JsonArray jsonArray = json.getJsonArray(JSON_HITTERS); 
        for(int i = 0; i< jsonArray.size();i++){
            JsonObject jo = jsonArray.getJsonObject(i); 
            Hitter h = new Hitter(); 
            //set the Data
            h.setTeam(jo.getString(JSON_TEAM));
            h.setLastName((jo.getString(JSON_LASTNAME)));
            h.setFirstName(jo.getString(JSON_FIRSTNAME));
            h.setNATION_OF_BIRTH(jo.getString(JSON_NATION_OF_BIRTH));
            h.setYEAR_OF_BIRTH(Integer.parseInt((jo.getString(JSON_YEAR_OF_BIRTH))));
            h.setNotes(jo.getString(JSON_NOTES));
            h.setAB(Integer.parseInt(jo.getString(JSON_AB)));
            h.setH(Integer.parseInt(jo.getString(JSON_H)));
            h.setR(Integer.parseInt(jo.getString(JSON_R)));
            h.setHR(Integer.parseInt(jo.getString(JSON_HR)));
            h.setQP(jo.getString(JSON_QP)); 
            h.setSB(Integer.parseInt(jo.getString(JSON_SB)));
            h.setRBI(Integer.parseInt(jo.getString(JSON_RBI)));
            //IF ZERO AT BATS 
            if(h.getAB() == 0){
                h.setBA(0);
            }
            else{
                h.setBA((double)(h.getH()/(double)h.getAB()));
            }
            //GIVE IT TO THE DATA MANAGER
            dataManager.addHitter(h); 
        }
        
    }

    public void loadPitchers(WDK_GUI gui, String jsonFilepath)throws IOException{
        DataManager dataManager = gui.getDataManager(); 
        JsonObject json = loadJSONFile(jsonFilepath); 
        JsonArray jsonArray = json.getJsonArray(JSON_PITCHERS); 
        
        for(int i = 0; i < jsonArray.size(); i++){
            JsonObject jo = jsonArray.getJsonObject(i); 
            Pitcher p = new Pitcher(); 
            
            p.setTeam(jo.getString(JSON_TEAM));
            p.setQP("P");
            p.setLastName((jo.getString(JSON_LASTNAME)));
            p.setFirstName(jo.getString(JSON_FIRSTNAME));
            p.setNATION_OF_BIRTH(jo.getString(JSON_NATION_OF_BIRTH));
            p.setYEAR_OF_BIRTH(Integer.parseInt((jo.getString(JSON_YEAR_OF_BIRTH))));
            p.setNotes(jo.getString(JSON_NOTES));
            p.setBB(Integer.parseInt(jo.getString(JSON_BB)));
            p.setER(Integer.parseInt(jo.getString(JSON_ER))); 
            p.setH(Integer.parseInt(jo.getString(JSON_H)));
            p.setIP(Double.parseDouble(jo.getString(JSON_IP)));
            p.setK(Integer.parseInt(jo.getString(JSON_K)));
            p.setSV(Integer.parseInt(jo.getString(JSON_SV)));
            p.setW(Integer.parseInt(jo.getString(JSON_W)));
            
            if(p.getIP() == 0){
                p.setWHIP(0);
                p.setERA(0);
            }
            else {
                p.setWHIP((p.getBB() + p.getH())/p.getIP());
                p.setERA((p.getER()/p.getIP())*9);
            }
            
            dataManager.addPitcher(p); 
            if(!dataManager.getMlbTeams().contains(p.getTeam())){
                dataManager.getMlbTeams().add(p.getTeam());
            }
        }
        Collections.sort(dataManager.getMlbTeams());
    }
    
    public void loadContracts(WDK_GUI gui, String jsonFilePath) throws IOException{
        DataManager dataManager = gui.getDataManager(); 
        JsonObject json = loadJSONFile(jsonFilePath);
        
        ArrayList<String> subjectsArray = loadArrayFromJSONFile(jsonFilePath, JSON_CONTRACTS);
        ArrayList<String> cleanedArray = new ArrayList();
        for (String s : subjectsArray) {
            // GET RID OF ALL THE QUOTE CHARACTERS
            s = s.replaceAll("\"", "");
            cleanedArray.add(s);
        }
        dataManager.setContracts(cleanedArray); 
    }
    public void saveDraft(WDK_GUI gui) throws IOException{
        Draft  draft = gui.getDataManager().getDraft();
        DataManager dataManager = gui.getDataManager();
        
        String draftName = draft.getName();
        String jsonFilePath = PATH_DRAFTS + draftName + JSON_EXT; 
        // INIT THE WRITER
        OutputStream os = new FileOutputStream(jsonFilePath);
        JsonWriter jsonWriter = Json.createWriter(os);      
        JsonArray freeAgents = makeFreeAgentArray(dataManager.getPlayers()); 
        JsonArray teams = makeTeamArray(dataManager.getTeams()); 
        JsonArray draftedPlayers = makeDraftedPlayersArray(dataManager.getDraftedPlayers());
        
        //SAVE ELEMENTS
        JsonObject draftJsonObject = Json.createObjectBuilder()
                .add(JSON_DRAFT, draft.getName())
                .add(JSON_FREE_AGENTS,freeAgents) 
                .add(JSON_TEAMS,teams)
                .add(JSON_DRAFTED_PLAYERS,draftedPlayers)
                .build(); 
        jsonWriter.writeObject(draftJsonObject);
    }
    public JsonArray makeDraftedPlayersArray(ObservableList<Player> draftedPlayers){
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for(Player p : draftedPlayers){
            jsb.add(makePlayerObject(p));
        }
        JsonArray jA = jsb.build();
        return jA;
    }
    
    public JsonArray makeFreeAgentArray(ArrayList<Player> players){
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (Player p : players) {
           jsb.add(makePlayerObject(p));
        }
        JsonArray jA = jsb.build();
        return jA;   
    }
    
    public JsonArray makeTeamArray(ObservableList<FantasyTeam> teams){
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for(FantasyTeam t : teams){
            jsb.add(makeTeamObject(t)); 
        }
        JsonArray jA =  jsb.build(); 
        return jA; 
    }
    
    public JsonObject makePlayerObject(Player p){
        JsonObject jso  = Json.createObjectBuilder()
                .add(JSON_LASTNAME,p.getLastName())
                .add(JSON_FIRSTNAME,p.getFirstName())
                .add(JSON_CONTRACT,p.getContract())
                .add(JSON_FQP,p.getFQP())
                .add(JSON_SALARY, p.getSalary())
                .build(); 
        return jso; 
    }
    
    public JsonObject makeTeamObject(FantasyTeam t){
        JsonArray roster = makeRosterObject(t.getRoster()); 
        JsonObject jso = Json.createObjectBuilder()
                .add(JSON_TEAM_NAME,t.getName()) 
                .add(JSON_ROSTER,roster)
                .add(JSON_OWNER,t.getOwner())
                .build(); 
        return jso; 
    }
    
    public JsonArray makeRosterObject(ObservableList<Player> roster){
        JsonArrayBuilder jsb = Json.createArrayBuilder();
            for(Player p : roster){
                jsb.add(makePlayerObject(p)); 
            }
            JsonArray jA =  jsb.build(); 
        return jA;
    }
    
    public void loadDraft(WDK_GUI gui, String jsonFilePath){
        
    }
}
