/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.gui.dialogs;

import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import static wdk.WDK_Startup_Constants.PATH_CSS;
import static wdk.WDK_Startup_Constants.PATH_IMAGES;
import wdk.data.FantasyTeam;
import wdk.data.Player;
import wdk.error.ErrorHandler;
import wdk.gui.MessageDialog;
import wdk.gui.WDK_GUI;

/**
 * When you edit a player this allows you to change fantasy teams and 
 * @author Sanjay
 */
public class EditPlayerDialog extends Stage{
    WDK_GUI gui; 
    ErrorHandler eh; 
    Scene playerDetailsScene; 
    
    static final String PRIMARY_STYLE_SHEET = PATH_CSS + "wdk_style.css";
    static final String CLASS_BORDERED_PANE = "bordered_pane"; 
    static final String CLASS_HEADING_LABEL = "heading_label"; 
    static final String CLASS_SUBHEADING_LABEL = "subheading_label"; 
    static final String CLASS_PROMPT_LABEL = "prompt_label";
    static final String PLAYER_IMAGE_FILEPATH  = "./images/players"; 
    static final String FLAG_IMAGE_FILEPATH  = PATH_IMAGES + "flags/"; 
    
    Image playerImage; 
    Image flagImage; 
    ImageView playerImageview; 
    ImageView flagImageview; 
    
    Label playerNameLabel; 
    Label positionsLabel; 
    Label fantasyTeamLabel; 
    Label teamPositionLabel; 
    Label contractLabel; 
    Label salaryLabel; 
    
    ComboBox positionComboBox; 
    ComboBox fantasyTeamComboBox; 
    ComboBox contractComboBox;
     
    TextField salaryTextField; 
    
    Button completeButton; 
    Button cancelButton; 
    
    
    //CONTAINERS
    BorderPane mainPane; 
    VBox deatBox; 
    GridPane detailPane; 
    MessageDialog msgDialog; 
    
    Player p; 
    
    //BUTTONS 
    //Button completeButton;
    //Button 
    public EditPlayerDialog(WDK_GUI gui,Player p, Stage owner){
        this.gui = gui;  
        this.p = p; 
        //ERROR HANDLER
        eh = ErrorHandler.getErrorHandler();
        eh.initMessageDialog(owner);
        msgDialog = new MessageDialog(owner,"ok"); 
        //MAKE IT MODAL
        initModality(Modality.WINDOW_MODAL);
        initOwner(owner);
        //PLAYER AND FLAG IMAGE 
        try{
            playerImage = new Image("/players/" + p.getLastName() + p.getFirstName() + ".jpg"); 
            playerImageview = new ImageView(playerImage);
            flagImage = new Image("/flags/" + p.getNATION_OF_BIRTH()+".png"); 
            flagImageview = new ImageView(flagImage); 
        }catch(Exception e){
            playerImage = new Image("/players/AAA_PhotoMissing.jpg"); 
            playerImageview = new ImageView(playerImage);   
            flagImage = new Image("/flags/Canada.png"); 
            flagImageview = new ImageView(flagImage); 
        }
        
        //LABELS 
        playerNameLabel = new Label(p.getFirstName() + " " + p.getLastName()); 
        positionsLabel = new Label(p.getQP()); 
        fantasyTeamLabel = new Label("Fantasy Team: "); 
        teamPositionLabel = new Label("Position: ");
        contractLabel = new Label("Contract: ");
        salaryLabel = new Label("Salary($): "); 
        
        //COMBOBOX AND TEXTFIELDS
        fantasyTeamComboBox = new ComboBox();
        
        fantasyTeamComboBox.setItems(gui.getDataManager().getTeams());
        positionComboBox = new ComboBox(); 
        String[] pos = p.getQP().split("_"); 
        for(String s : pos){
            positionComboBox.getItems().add(s); 
        }
        if(positionComboBox.getItems().contains("1B")||positionComboBox.getItems().contains("3B")){
            positionComboBox.getItems().add("CI");
        }
        
        if(positionComboBox.getItems().contains("2B")||positionComboBox.getItems().contains("SS")){
            positionComboBox.getItems().add("MI");
        }
        
        if(positionComboBox.getItems().contains("1B")||positionComboBox.getItems().contains("3B")
           ||positionComboBox.getItems().contains("2B")||positionComboBox.getItems().contains("SS")  
           ||positionComboBox.getItems().contains("OF")){
            positionComboBox.getItems().add("U");
        }
        contractComboBox = new ComboBox(); 
        contractComboBox.setItems(FXCollections.observableList(gui.getDataManager().getContracts())); 
        salaryTextField = new TextField(); 
         
        detailPane = new GridPane(); 
        detailPane.setPadding(new Insets(10, 20, 20, 20));
        detailPane.setHgap(10);
        detailPane.setVgap(10);
        
        completeButton = new Button("Complete"); 
        cancelButton =  new Button("Cancel" ); 
        
        detailPane.add(playerImageview,1,1);
        detailPane.add(flagImageview,2,1); 
        detailPane.add(playerNameLabel,2,2); 
        detailPane.add(positionsLabel,2,3); 
        detailPane.add(fantasyTeamLabel,1,4); 
        detailPane.add(fantasyTeamComboBox,2,4); 
        detailPane.add(teamPositionLabel,1,5); 
        detailPane.add(positionComboBox,2,5);
        detailPane.add(contractLabel,1,6); 
        detailPane.add(contractComboBox,2,6); 
        detailPane.add(salaryLabel,1,7);
        detailPane.add(salaryTextField,2,7); 
        detailPane.add(completeButton,1,8); 
        detailPane.add(cancelButton,2,8); 
        
        playerDetailsScene = new Scene(detailPane);
        playerDetailsScene.getStylesheets().add(PRIMARY_STYLE_SHEET); 
        playerNameLabel.getStyleClass().add(CLASS_SUBHEADING_LABEL); 
        positionsLabel.getStyleClass().add(CLASS_PROMPT_LABEL); 
        fantasyTeamLabel.getStyleClass().add(CLASS_PROMPT_LABEL); 
        teamPositionLabel.getStyleClass().add(CLASS_PROMPT_LABEL); 
        salaryLabel.getStyleClass().add(CLASS_PROMPT_LABEL); 
        contractLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        this.setScene(playerDetailsScene);
        this.setTitle("Edit Player");
        initHandlers(); 
    }
    
    public void showDialog(){
        this.show(); 
    }
    
    private void initHandlers(){
        cancelButton.setOnAction(e -> {this.close();});
        
        completeButton.setOnAction(e -> {
            if(contractComboBox.getSelectionModel().getSelectedItem().equals("X")){
                //ADD THE PLAYER TO THE TAXI SQUAD
                ((FantasyTeam)fantasyTeamComboBox.getSelectionModel().getSelectedItem()).getTaxiSquad().add(p); 
                p.setFantasyTeam(((FantasyTeam)fantasyTeamComboBox.getSelectionModel().getSelectedItem()));
                p.setContract("X");
                p.setDrafted(true);
                gui.getDataManager().getPlayers().remove(p); 
                gui.getDataManager().getDisplayList().remove(p); 
                this.close();
            } 
            else{
            try{
                int salary = Integer.parseInt(salaryTextField.getText());
                if(salary >((FantasyTeam)fantasyTeamComboBox.getSelectionModel().getSelectedItem()).calcMaxBet()){
                    msgDialog.show("Cannot bid over $" +((FantasyTeam)fantasyTeamComboBox.getSelectionModel().getSelectedItem()).calcMaxBet());
                }
                else{
                boolean added =  ((FantasyTeam)fantasyTeamComboBox.getSelectionModel().getSelectedItem()).addPlayer(p, (String)positionComboBox.getSelectionModel().getSelectedItem());
                //EXCEUTE IF THE PLAYER WAS SUCESSFULLY ADDED
                if(added){    
                    //WAS THE PLAYER ON A TEAM BEFORE?
                    if(p.isDrafted()&&added){
                        //delete it from the old team
                        p.getfTeam().removePlayer(p); 
                    } 
                    p.setFQP((String)positionComboBox.getSelectionModel().getSelectedItem());
                
                    p.setFantasyTeam((FantasyTeam)fantasyTeamComboBox.getSelectionModel().getSelectedItem());
                    p.setSalary(Integer.parseInt(salaryTextField.getText()));
                    p.getfTeam().setPayroll(p.getfTeam().getPayroll() - p.getSalary());
                    p.setContract((String)contractComboBox.getSelectionModel().getSelectedItem());
                    // IF S2 UPDATE THE SUMMARY
                    if(p.getContract().equals("S2")) gui.getDataManager().getDraftedPlayers().add(p);
                    if(!p.isDrafted()){
                        p.setDrafted(true);
                        gui.getDataManager().getPlayers().remove(p); 
                        gui.getDataManager().getDisplayList().remove(p); 
                    }
                    this.close();
                }
                else{
                    msgDialog.show("Cannot transfer this player");
                }
               }
            }catch(Exception es){
                msgDialog.show("Please Complete all Fields");
            }
           }
        });
        
        fantasyTeamComboBox.setOnAction(e -> {
            FantasyTeam team = (FantasyTeam)fantasyTeamComboBox.getSelectionModel().getSelectedItem();
            if(!team.isFull()){
                contractComboBox.getItems().remove("X"); 
            }
            if(team.isFull()){
                contractComboBox.setItems(FXCollections.observableList(gui.getDataManager().getContracts())); 
            }
           
        });
    }
    public void freeAgent(Player p){
        gui.getDataManager().getPlayers().add(p);
        gui.getDataManager().getDisplayList().add(p); 
        p.setFQP("");
        p.getfTeam().getRoster().remove(p); 
        //NOT DRAFTED ANYMORE
        p.setDrafted(false); 
    }
}
