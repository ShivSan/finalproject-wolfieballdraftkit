/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.gui.dialogs;

import java.io.IOException;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import static wdk.WDK_Startup_Constants.*;
import wdk.data.Player;
import wdk.error.ErrorHandler;
import wdk.gui.WDK_GUI;

/**
 * This Dialog pops up when you want to add a player to the free agents. 
 * @author Sanjay
 */
public class AddPlayerDialog extends Stage{
    WDK_GUI gui; 
    ErrorHandler eh; 
    Scene addPlayerScene; 
    
    static final String PRIMARY_STYLE_SHEET = PATH_CSS + "wdk_style.css";
    
    GridPane pane; 
    
    Label playerDetailsLabel; 
    Label firstNameLabel;
    Label lastNameLabel; 
    Label proTeamLabel; 
    
    TextField firstNameTextField; 
    TextField lastNameTextField; 
    ComboBox proTeamComboBox; 
        
    CheckBox firstBaseCheckBox; 
    CheckBox secondBaseCheckBox; 
    CheckBox thirdBaseCheckBox; 
    CheckBox shortStopCheckBox; 
    CheckBox cornerInfieldCheckBox; 
    CheckBox middleInfieldCheckBox; 
    CheckBox outFieldCheckBox; 
    CheckBox pitcherCheckBox; 
    HBox positionBox; 
    
    Button completeButton; 
    Button cancelButton; 
    
    Player p; 
    
    public AddPlayerDialog(Stage owner, WDK_GUI gui){
        this.gui = gui; 
        //ERROR HANDLER
        eh = ErrorHandler.getErrorHandler();
        eh.initMessageDialog(owner);
        
        //MAKE IT MODAL
        initModality(Modality.WINDOW_MODAL);
        initOwner(owner);
        
        //INIT THE LABELS
        playerDetailsLabel = new Label("Player Details"); 
        firstNameLabel = new Label("First Name");
        lastNameLabel = new Label("Last Name");
        proTeamLabel = new Label("Pro Team"); 
        //OUR TEAM COMBO BOX 
        proTeamComboBox = new ComboBox();
        proTeamComboBox.setItems(gui.getDataManager().getMlbTeams());
        //TEXT FEILDS
        lastNameTextField = new TextField(); 
        firstNameTextField = new TextField(); 
 
        //NOW THE POSITON CHEKBOXES
        firstBaseCheckBox = new CheckBox(POSITION_1B); 
        secondBaseCheckBox = new CheckBox(POSITION_2B);
        thirdBaseCheckBox  = new CheckBox(POSITION_3B);
        shortStopCheckBox = new CheckBox(POSITION_SS);
        cornerInfieldCheckBox = new CheckBox(POSITION_CI);
        middleInfieldCheckBox = new CheckBox(POSITION_MI);
        outFieldCheckBox  = new CheckBox(POSITION_OF);
        pitcherCheckBox = new CheckBox(POSITION_P);
        //ADD THEM TO THE HBOX 
        positionBox = new HBox(); 
        positionBox.getChildren().add(firstBaseCheckBox);
        positionBox.getChildren().add(secondBaseCheckBox);
        positionBox.getChildren().add(shortStopCheckBox);
        positionBox.getChildren().add(thirdBaseCheckBox);
        positionBox.getChildren().add(middleInfieldCheckBox);
        positionBox.getChildren().add(cornerInfieldCheckBox);
        positionBox.getChildren().add(outFieldCheckBox);
        positionBox.getChildren().add(pitcherCheckBox); 
        
        //NOW THE BUTTONS 
        completeButton = new Button("Complete"); 
        cancelButton = new Button("Cancel"); 
        
        //INTO THE GIRDPANE
        pane = new GridPane();
        pane.setPadding(new Insets(10, 20, 20, 20));
        pane.setHgap(10);
        pane.setVgap(10); 
        pane.add(playerDetailsLabel,0,1,1,1); 
        pane.add(lastNameLabel,0,2,1,1);      
        pane.add(lastNameTextField,1,2,1,1);
        pane.add(firstNameLabel,0,3,1,1); 
        pane.add(firstNameTextField,1,3,1,1);
        pane.add(proTeamLabel,0,4,1,1); 
        pane.add(proTeamComboBox,1,4,1,1); 
        pane.add(positionBox,1,5,1,1); 
        pane.add(completeButton,1,6,1,1);
        pane.add(cancelButton,2,6,1,1); 
        
        addPlayerScene = new Scene(pane); 
        addPlayerScene.getStylesheets().add(PRIMARY_STYLE_SHEET); 
                //SET STYLE CLASSES
        playerDetailsLabel.getStyleClass().add("heading_label");
        firstNameLabel.getStyleClass().add("prompt_label"); 
        lastNameLabel.getStyleClass().add("prompt_label"); 
        proTeamLabel.getStyleClass().add("prompt_label");
        this.setScene(addPlayerScene); 
        initHandlers(); 
    }
    
   public void showDialog(){
        this.show(); 
    }
   
   private void initHandlers(){
       completeButton.setOnAction(e -> { 
           try{
               Player p = createPlayer(); 
            gui.getDataManager().getPlayers().add(p);
            this.close();
           }catch(Exception ioe){
               eh.handleAddPlayerError(); 
           }
       });
       
       cancelButton.setOnAction(e ->{
               this.close();
        });
   }
   
   private Player createPlayer()throws IOException{
       p  = new Player(); 
       String lastName = lastNameTextField.getText(); 
       String firstName = firstNameTextField.getText();
       if(lastNameTextField.getText().equals("")|| firstNameTextField.getText().equals("")){
           eh.handleAddPlayerError();
       }
       String QP = "";
       for(int i = 0; i < positionBox.getChildren().size(); i++){
           CheckBox box = (CheckBox)positionBox.getChildren().get(i);
           if(box.isSelected()){
               QP  = QP + "_" + box.getText(); 
           }
            
       }
       try{
        QP = QP.substring(1);
       }catch(Exception e){
           eh.handleAddPlayerError();
       }
       
       //SET THE FIELDS
       p.setLastName(lastName);
       p.setFirstName(firstName);
       p.setQP(QP); 
       return p; 
   }
 
}
