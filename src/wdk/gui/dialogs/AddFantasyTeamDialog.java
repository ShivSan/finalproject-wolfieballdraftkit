/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.gui.dialogs;

import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import static wdk.WDK_Startup_Constants.PATH_CSS;
import wdk.data.FantasyTeam;
import wdk.error.ErrorHandler;
import wdk.gui.MessageDialog;
import wdk.gui.screens.FantasyTeamScreen;

/**
 *
 * @author Sanjay
 */
public class AddFantasyTeamDialog extends Stage{
    FantasyTeamScreen fantasyTeamScreen;  
    ErrorHandler eh; 
    Scene mainScene; 
    GridPane pane; 
    static final String PRIMARY_STYLE_SHEET = PATH_CSS + "wdk_style.css";
    //LABELS
    Label detailsLabel; 
    Label nameLabel; 
    Label ownerLabel;
    
    //TEXTFEIDLDS
    TextField nameTextField; 
    TextField ownerTextField; 
    
    //BUTTONS 
    Button completeButton; 
    Button cancelButton; 
    
    MessageDialog msgDialog; 
    
    public AddFantasyTeamDialog(Stage owner, FantasyTeamScreen screen){
        msgDialog = new MessageDialog(this,"ok"); 
        //ERROR HANDLER
        eh = ErrorHandler.getErrorHandler();
        eh.initMessageDialog(owner);
        fantasyTeamScreen = screen; 
        //MAKE IT MODAL
        initModality(Modality.WINDOW_MODAL);
        initOwner(owner);
        
        //LABELS
        detailsLabel = new Label("Fantasy Team Details");
        nameLabel = new Label("Team Name: "); 
        ownerLabel = new Label("Owner: ");
        
        //TEXTFIELDS
        nameTextField = new TextField(); 
        ownerTextField = new TextField();
        
        //Buttons 
        completeButton = new Button("Complete"); 
        cancelButton = new Button("Cancel");
        
        //ADD IT TO THE PANE 
        pane = new GridPane();
        pane.setPadding(new Insets(10, 20, 20, 20));
        
        pane.add(detailsLabel,1,1); 
        pane.add(nameLabel,1,2); 
        pane.add(ownerLabel,1,3); 
        pane.add(completeButton,1,4); 
        pane.add(nameTextField,2,2); 
        pane.add(ownerTextField,2,3); 
        pane.add(cancelButton,2,4); 
        
        mainScene = new Scene(pane); 
        mainScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        
        detailsLabel.getStyleClass().add("heading_label"); 
        nameLabel.getStyleClass().add("prompt_label");
        ownerLabel.getStyleClass().add("prompt_label");
        initHandlers(); 
        
        this.setScene(mainScene);
    }

    private void initHandlers(){
        cancelButton.setOnAction(e -> {
            this.close();
        });
        
        completeButton.setOnAction( e->{
            FantasyTeam team = new FantasyTeam();
            if(nameTextField.getText().equals("")||ownerTextField.getText().equals("")){
                msgDialog.show("Please fill out all the text fields"); 
            }
            else{
                team.setName(nameTextField.getText());
                team.setOwner(ownerTextField.getText());
                fantasyTeamScreen.getDataManager().addTeam(team);
                fantasyTeamScreen.getTeamsComboBox().getSelectionModel().select(team);
                this.close();
            }
        }); 
    }
    
}
