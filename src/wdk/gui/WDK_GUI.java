
package wdk.gui;

import java.io.IOException;
import java.util.ArrayList;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Orientation;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.util.Callback;
import properties_manager.PropertiesManager;
import wdk.WDK_PropertyType;
import static wdk.WDK_Startup_Constants.*;
import wdk.controller.FileController;
import wdk.data.DataManager;
import wdk.data.FantasyTeam;
import wdk.data.Hitter;
import wdk.data.Pitcher;
import wdk.data.Player;
import wdk.error.ErrorHandler;
import wdk.file.DraftFileManager;
import wdk.gui.dialogs.AddPlayerDialog;
import wdk.gui.dialogs.EditPlayerDialog;
import wdk.gui.screens.DraftSummaryScreen;
import wdk.gui.screens.FantasyStandingsScreen;
import wdk.gui.screens.FantasyTeamScreen;
import wdk.gui.screens.MLBScreen;

/**
 * THIS IS IT this is the main gui class that has the main components of the the application 
 * display
 * @author Sanjay
 */
public class WDK_GUI {
    //Constants used for settings
    static final String PRIMARY_STYLE_SHEET = PATH_CSS + "wdk_style.css";
    static final String CLASS_BORDERED_PANE = "bordered_pane"; 
    static final String CLASS_HEADING_LABEL = "heading_label"; 
    static final String CLASS_SUBHEADING_LABEL = "subheading_label"; 
    static final String CLASS_PROMPT_LABEL = "prompt_label"; 
    static final String CLASS_SCREEN_PANE = "screen_pane";
    static final int LARGE_TEXT_FIELD_LENGTH = 20;
    static final int SMALL_TEXT_FIELD_LENGTH = 5;
    //ERROR HANDLER
    ErrorHandler eh; 
    //FILE MANAGERS 
    DraftFileManager draftFileManager;
    
    //FILE CONTROLLER 
    FileController fileController; 
    
    //DATA MANGER
    DataManager dataManager; 
    
    //APPLICATION WINDOW
    Stage primaryStage;
    
    //MAIN SCEENE GRAPH
    Scene primaryScene; 
    
    //THIS IS THE MAIN MAIN PANES THAT THE APPLICATION WILL BE IN
    //DEPENDING ON THE SCREEN 
    BorderPane wdkPane; 
    BorderPane centerPane;
    
    BorderPane playerPane;
    BorderPane mlbPane; 
    BorderPane fantasyTeamsPane; 
    BorderPane fantasyStandingsPane;
    FantasyTeamScreen fantasyTeamsScreen;  
    MLBScreen mlbScreen; 
    DraftSummaryScreen draftScreen; 
    FantasyStandingsScreen fantasyStandingsScreen; 
    BorderPane draftSummaryPane; 
    
    //FILE TOOBAR FOR THE TOP OF THE SCREEN
    FlowPane topToolbarPane; 
    Button newDraftButton;
    Button loadDraftButton;
    Button saveDraftButton;
    Button exportDraftButton;
    Button exitButton;
    
    //BOTTOM TOOLBAR TO HOLD THE PAGE CONTROLS
    FlowPane bottomToolbarPane; 
    Button playersScreenButton; 
    Button fantasyTeamsScreenButton; 
    Button fantasyStandingsScreenButton; 
    Button mlbTeamsScreenButton; 
    Button draftSummaryScreenButton; 
    
    //MAKE THE WORKSPACE A BORDERED PANE
    BorderPane workspacePane; 
    boolean workspaceActivated; 
    ScrollPane workspaceScrollPane; 
    
    //TOP WORKSPACE PANE FOR CONTROLS 
    VBox topWorkspacePane; 
    SplitPane topWorkspaceSplitPane; 
    Label pageHeadingLabel;
    
    //THESE ARE THE CONTROLS FOR THE PLAYERS PAGE 
    //THESE GO IN THE TOP WORKSPACE PANE
    BorderPane topPlayerControls;  
    FlowPane searchPlayerControls; 
    Button addPlayerButton; 
    Button deletePlayerButton; 
    TextField searchTextField; 
    Label searchLabel; 
    //MAKE A PANE FOR THE RADIO  BUTTONS TO TOGGLE 
    //FOR SEARCH CRITERIA 
    HBox positionButtonsPane; 
    ArrayList <RadioButton> radioButtons; 
    RadioButton allRadioButton; 
    RadioButton catcherRadioButton; 
    RadioButton firstBaseRadioButton; 
    RadioButton secondBaseRadioButton; 
    RadioButton thirdBaseRadioButton; 
    RadioButton cornerInfieldRadioButton; 
    RadioButton middleInfieldRadioButton; 
    RadioButton shortStopRadioButton; 
    RadioButton outfieldRadioButton; 
    RadioButton pitcherRadioButton; 
    RadioButton utilityRadioButton; 
    //CONTROLS FOR THE PLAYER TABLE 
    TableView<Player> playersTable; 
    TableColumn lastNameColumn; 
    TableColumn firstNameColumn; 
    TableColumn proTeamColumn; 
    TableColumn qualifiedPositionsColumn; 
    TableColumn yearOfBirthColumn; 
    TableColumn runsWinsColumn; 
    TableColumn homeRunsSavesColumn; 
    TableColumn rbiStrikeOutsColumn; 
    TableColumn sbERAColumn; 
    TableColumn baWHIPColumn; 
    TableColumn estValColumn; 
    TableColumn noteColumn; 
    
    //COLUMN HEADERS
    static final String COL_LAST_NAME = "Last Name";
    static final String COL_FIRST_NAME = "First Name"; 
    static final String COL_PRO_TEAM = "Pro Team"; 
    static final String COL_QP = "Positions"; 
    static final String COL_YEAR_OF_BIRTH = "Year Od Birth"; 
    static final String COL_RUNS = "R"; 
    static final String COL_HR = "HR"; 
    static final String COL_RBI = "RBI"; 
    static final String COL_SB = "SB"; 
    static final String COL_BA = "BA"; 
    static final String COL_ESTIMATED_VALUE = "Esitimated Vaule"; 
    static final String COL_NOTES = "Notes"; 
    
    //MESSAGE DIALOGS
    MessageDialog messageDialog; 
    YesNoCancelDialog yesNoCancelDialog; 
    AddPlayerDialog addPlayerDialog; 
    EditPlayerDialog editPlayerDialog; 
    
    //FINALLY WE HAVE THE CONTRUCTOR
    public WDK_GUI(Stage initPrimaryStage,DraftFileManager fileManager){
        primaryStage = initPrimaryStage; 
        draftFileManager = fileManager;
        dataManager = new DataManager(); 
    }
    //********************//
    //GETTERS AND SETTERS//
    //*******************//

    public FileController getFileController() {
        return fileController;
    }

    public DataManager getDataManager() {
        return dataManager;
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public TableView<Player> getPlayersTable() {
        return playersTable;
    }
    
    
    //THIS IS WHERE THE MAGIC HAPPENS
    public void initGUI(String windowTitle, ArrayList<String> hitters, 
            ArrayList<String> picthers)throws IOException{
        
        initDialogs();
        
        initTopToolbar(); 
        
        initBottomToolbar(); 
        
        initEventHandlers(); 
        
        initWindow(APP_TITLE); 
        
        initPlayersScreen(dataManager.getHitters(), dataManager.getPicthers()); 
        
        initMLBScreen();
        
        initFantasyTeamsScreen();
        
        initFantasyStandingsScreen(); 
        
        initDraftSummaryScreen(); 
        
    }
    
    //METHODS FOR INTIIATING THE CONTROLS WILL BE PRIVATE
    private void initDialogs(){
        messageDialog = new MessageDialog(primaryStage,CLOSE_BUTTON_LABEL); 
        yesNoCancelDialog = new YesNoCancelDialog(primaryStage);
        addPlayerDialog = new AddPlayerDialog(primaryStage,this); 
    }
    
    //CREATE THE TOOLBAR AND SET VSISBLE BUTTONS
    private void initTopToolbar(){
        topToolbarPane = new FlowPane();
        //INIT THE BUTTONS 
        newDraftButton = initChildButton(topToolbarPane,WDK_PropertyType.NEW_DRAFT_ICON,WDK_PropertyType.NEW_DRAFT_TOOLTIP,false);
        saveDraftButton = initChildButton(topToolbarPane,WDK_PropertyType.SAVE_DRAFT_ICON,WDK_PropertyType.SAVE_DRAFT_TOOLTIP,false);
        loadDraftButton = initChildButton(topToolbarPane,WDK_PropertyType.LOAD_DRAFT_ICON,WDK_PropertyType.LOAD_DRAFT_TOOLTIP,true);
        exportDraftButton = initChildButton(topToolbarPane,WDK_PropertyType.EXPORT_DRAFT_ICON,WDK_PropertyType.EXPORT_DRAFT_TOOLTIP,true);
        exitButton = initChildButton(topToolbarPane,WDK_PropertyType.EXIT_ICON,WDK_PropertyType.EXIT_TOOLTIP,false);
        topToolbarPane.setOrientation(Orientation.HORIZONTAL);
    }
    
    //CREATE THE TOOOLBAR TO ENABLE SCREEN SWITCHING 
    private void initBottomToolbar(){
        bottomToolbarPane = new FlowPane();
        
        playersScreenButton = initChildButton(bottomToolbarPane,WDK_PropertyType.FREE_AGENT_ICON,WDK_PropertyType.PLAYER_TOOLTIP,true); 
        fantasyTeamsScreenButton = initChildButton(bottomToolbarPane,WDK_PropertyType.FANTASY_TEAMS_ICON,WDK_PropertyType.FANTASY_TEAMS_TOOLTIP,true);
        fantasyStandingsScreenButton = initChildButton(bottomToolbarPane,WDK_PropertyType.FANTASY_STANDINGS_ICON,WDK_PropertyType.FANTASY_STANDINGS_TOOLTIP,true);
        mlbTeamsScreenButton = initChildButton(bottomToolbarPane,WDK_PropertyType.MLB_TEAMS_ICON,WDK_PropertyType.MLB_TEAMS_TOOLTIP,true);
        draftSummaryScreenButton = initChildButton(bottomToolbarPane,WDK_PropertyType.DRAFT_SUMMARY_ICON,WDK_PropertyType.DRAFT_SUMMARY_TOOLTIP,true); 
    }
    
    //CREATE THE PLAYERS SCREEN 
    public void initPlayersScreen(ArrayList<Player> hitters, ArrayList<Player> picthers){
        dataManager.createPlayers();
         playerPane = new BorderPane();
         //SET THE STYLE
         playerPane.getStyleClass().add(CLASS_SCREEN_PANE); 
         //SET UP THE TOP GRIDBOX
         topPlayerControls = new BorderPane(); 
         
         //Heading label
         pageHeadingLabel = new Label("Avaliable Players");
         pageHeadingLabel.getStyleClass().add(CLASS_HEADING_LABEL); 
         
         
         searchTextField = new TextField(); 
         searchLabel = new Label("Search: "); 
         searchLabel.getStyleClass().add(CLASS_PROMPT_LABEL); 
         
         //ADD THE CONTROLS TO THE TOP PANE 
         topPlayerControls.setTop(pageHeadingLabel);
          
         searchPlayerControls = new FlowPane(); 
         addPlayerButton = initChildButton(searchPlayerControls, WDK_PropertyType.ADD_ICON, WDK_PropertyType.EXPORT_DRAFT_TOOLTIP,false);
         deletePlayerButton = initChildButton(searchPlayerControls, WDK_PropertyType.MINUS_ICON, WDK_PropertyType.EXPORT_DRAFT_TOOLTIP,false);
         searchPlayerControls.getChildren().add(searchLabel); 
         searchPlayerControls.getChildren().add(searchTextField);
         
         topPlayerControls.setBottom(searchPlayerControls); 
         
         playerPane.setTop(topPlayerControls); 
         
         //Create the radio Buttons 
         positionButtonsPane = new HBox();  
         allRadioButton = initRadioButton(positionButtonsPane,"All\t"); 
         catcherRadioButton = initRadioButton(positionButtonsPane,POSITION_C); 
         firstBaseRadioButton = initRadioButton(positionButtonsPane,POSITION_1B); 
         secondBaseRadioButton = initRadioButton(positionButtonsPane,POSITION_2B);
         shortStopRadioButton = initRadioButton(positionButtonsPane,POSITION_SS); 
         thirdBaseRadioButton = initRadioButton(positionButtonsPane,POSITION_3B); 
         cornerInfieldRadioButton = initRadioButton(positionButtonsPane,POSITION_CI);
         middleInfieldRadioButton = initRadioButton(positionButtonsPane,POSITION_MI);
         outfieldRadioButton = initRadioButton(positionButtonsPane,POSITION_OF);
         pitcherRadioButton = initRadioButton(positionButtonsPane,POSITION_P);
         //STORE THEM IN A LIST TO HELP US SEARCH 
         radioButtons = new ArrayList(); 
         radioButtons.add(allRadioButton); 
         radioButtons.add(catcherRadioButton);
         radioButtons.add(firstBaseRadioButton);
         radioButtons.add(secondBaseRadioButton);
         radioButtons.add(thirdBaseRadioButton);
         radioButtons.add(shortStopRadioButton); 
         radioButtons.add(cornerInfieldRadioButton);
         radioButtons.add(middleInfieldRadioButton);
         radioButtons.add(outfieldRadioButton);
         radioButtons.add(pitcherRadioButton);
         
         //STYLE IT AND ADD IT TO THE PLAYER PANE 
         positionButtonsPane.getStyleClass().add("radio_button_pane"); 
         playerPane.setCenter(positionButtonsPane);

         //NOW THE TABLE 
         playersTable = new TableView(); 
         lastNameColumn = new TableColumn(COL_LAST_NAME); 
         firstNameColumn = new TableColumn(COL_FIRST_NAME);
         proTeamColumn = new TableColumn(COL_PRO_TEAM); 
         qualifiedPositionsColumn = new TableColumn(COL_QP);
         yearOfBirthColumn = new TableColumn(COL_YEAR_OF_BIRTH); 
         runsWinsColumn = new TableColumn(COL_RUNS + "/W"); 
         homeRunsSavesColumn = new TableColumn(COL_HR + "/SV"); 
         rbiStrikeOutsColumn = new TableColumn(COL_RBI+"/K");
         sbERAColumn = new TableColumn(COL_SB +  "/ERA");
         baWHIPColumn = new TableColumn(COL_BA + "/WHIP");
         estValColumn = new TableColumn(COL_ESTIMATED_VALUE);
         noteColumn = new TableColumn(COL_NOTES);
         
         //LINK THE COULUMNS TO THE DATA
         lastNameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("lastName"));
         firstNameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("firstName"));
         proTeamColumn.setCellValueFactory(new PropertyValueFactory<String, String>("team"));
         yearOfBirthColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("YEAR_OF_BIRTH"));
         qualifiedPositionsColumn.setCellValueFactory(new PropertyValueFactory<String, String>("QP"));
         runsWinsColumn.setCellValueFactory(new Callback(){
            @Override 
            public ObservableValue call(Object param){
                TableColumn.CellDataFeatures x = (TableColumn.CellDataFeatures) param; 
                if(x.getValue().getClass().equals((new Hitter()).getClass())) return new SimpleIntegerProperty(((Hitter)x.getValue()).getR());
                else if(x.getValue().getClass().equals((new Pitcher()).getClass())) return new SimpleIntegerProperty(((Pitcher)x.getValue()).getW());
                else return new SimpleIntegerProperty(0);
                }});
         rbiStrikeOutsColumn.setCellValueFactory(new Callback(){
            @Override 
            public ObservableValue call(Object param){
                TableColumn.CellDataFeatures x = (TableColumn.CellDataFeatures) param; 
                if(x.getValue().getClass().equals((new Hitter()).getClass())) return new SimpleIntegerProperty(((Hitter)x.getValue()).getRBI());
                else if(x.getValue().getClass().equals((new Pitcher()).getClass())) return new SimpleIntegerProperty(((Pitcher)x.getValue()).getK());
                else return new SimpleIntegerProperty(0);
                }});
         sbERAColumn.setCellValueFactory(new Callback(){
            @Override 
            public ObservableValue call(Object param){
                TableColumn.CellDataFeatures x = (TableColumn.CellDataFeatures) param; 
                if(x.getValue().getClass().equals((new Hitter()).getClass())) return new SimpleIntegerProperty(((Hitter)x.getValue()).getSB());
                else if(x.getValue().getClass().equals((new Pitcher()).getClass())) return new SimpleDoubleProperty(((Pitcher)x.getValue()).getERA());
                else return new SimpleIntegerProperty(0);
                }});
         baWHIPColumn.setCellValueFactory(new Callback(){
            @Override 
            public ObservableValue call(Object param){
                TableColumn.CellDataFeatures x = (TableColumn.CellDataFeatures) param; 
                if(x.getValue().getClass().equals((new Hitter()).getClass())) return new SimpleDoubleProperty(((Hitter)x.getValue()).getBA());
                else if(x.getValue().getClass().equals((new Pitcher()).getClass())) return new SimpleDoubleProperty(((Pitcher)x.getValue()).getWHIP());
                else return new SimpleIntegerProperty(0);
                }});
         noteColumn.setCellValueFactory(new PropertyValueFactory<String, String>("notes"));
         homeRunsSavesColumn.setCellValueFactory(new Callback(){
            @Override 
            public ObservableValue call(Object param){
                TableColumn.CellDataFeatures x = (TableColumn.CellDataFeatures) param; 
                if(x.getValue().getClass().equals((new Hitter()).getClass())) return new SimpleIntegerProperty(((Hitter)x.getValue()).getHR());
                else if(x.getValue().getClass().equals((new Pitcher()).getClass())) return new SimpleIntegerProperty(((Pitcher)x.getValue()).getSV());
                else return new SimpleIntegerProperty(0);
                }});
         estValColumn.setCellValueFactory(new Callback(){
            @Override 
            public ObservableValue call(Object param){
                TableColumn.CellDataFeatures x = (TableColumn.CellDataFeatures) param; 
                Player p = (Player)x.getValue(); 
                return new SimpleIntegerProperty(calcEstimatedVal(p)); 
                }});
        //ALLOW EDITING FOR THE NOTE COLUMN
         playersTable.setEditable(true);
         Callback<TableColumn, TableCell> cellFactory =
             new Callback<TableColumn, TableCell>() {
                 public TableCell call(TableColumn p) {
                    return new EditingCell();
                 }
             };
         noteColumn.setCellFactory(cellFactory);
         
         playersTable.getColumns().add(lastNameColumn);
         playersTable.getColumns().add(firstNameColumn);
         playersTable.getColumns().add(proTeamColumn);
         playersTable.getColumns().add(qualifiedPositionsColumn);
         playersTable.getColumns().add(yearOfBirthColumn);
         playersTable.getColumns().add(runsWinsColumn);
         playersTable.getColumns().add(homeRunsSavesColumn);
         playersTable.getColumns().add(rbiStrikeOutsColumn);
         playersTable.getColumns().add(sbERAColumn);
         playersTable.getColumns().add(baWHIPColumn);
         playersTable.getColumns().add(estValColumn);
         playersTable.getColumns().add(noteColumn);
         
         dataManager.initDisplayList(); 
         playersTable.setItems(dataManager.getDisplayList()); 
         //ADD IT TO THE PLAYER PANE
         initPlayerSearchHandlers(); 
         playerPane.setBottom(playersTable);
         // THE PLAYER SCREEN SHOULD COME UP FIRST 
         centerPane.setCenter(playerPane);
    }
    
    //Create the  mlb pane
    private void initMLBScreen(){
        /**mlbPane = new BorderPane();
        mlbPane.getStyleClass().add(CLASS_SCREEN_PANE); 
        pageHeadingLabel = new Label("MLB Teams");
        pageHeadingLabel.getStyleClass().add(CLASS_HEADING_LABEL);
        mlbPane.setTop(pageHeadingLabel); */
        mlbScreen = new MLBScreen(this.getDataManager());
    }
    
    private void initFantasyTeamsScreen(){
        /**fantasyTeamsPane = new BorderPane();
        fantasyTeamsPane.getStyleClass().add(CLASS_SCREEN_PANE); 
        pageHeadingLabel = new Label("Fantasy Teams");
        pageHeadingLabel.getStyleClass().add(CLASS_HEADING_LABEL);
        fantasyTeamsPane.setTop(pageHeadingLabel); */
        fantasyTeamsScreen = new FantasyTeamScreen(this); 
        
    }
    
        private void initFantasyStandingsScreen(){
       /** fantasyStandingsPane = new BorderPane();
        fantasyStandingsPane.getStyleClass().add(CLASS_SCREEN_PANE); 
        pageHeadingLabel = new Label("Fantasy Standings");
        pageHeadingLabel.getStyleClass().add(CLASS_HEADING_LABEL);
        fantasyStandingsPane.setTop(pageHeadingLabel); 
        * */
        fantasyStandingsScreen = new FantasyStandingsScreen(this.dataManager); 
    }
        
        private void initDraftSummaryScreen(){
            /**draftSummaryPane = new BorderPane();
            draftSummaryPane.getStyleClass().add(CLASS_SCREEN_PANE); 
            pageHeadingLabel = new Label("Draft Summary");
            pageHeadingLabel.getStyleClass().add(CLASS_HEADING_LABEL);
            draftSummaryPane.setTop(pageHeadingLabel); */
            draftScreen = new DraftSummaryScreen(this); 
        }
    
    private void initPlayerSearchHandlers(){
        searchTextField.setOnAction(e -> {
            String search = searchTextField.getText();
            searchByText(search); 
        });
        
        allRadioButton.setOnAction(e ->{
            searchTextField.clear();
            searchByText(""); 
            if(allRadioButton.isSelected()){
                //DESECT THE OTHER RADIO BUTTONS 
                for(int i = 1; i < radioButtons.size(); i++){
                    radioButtons.get(i).setSelected(false);
                }
            }
        });
        
        firstBaseRadioButton.setOnAction(e -> { 
            dataManager.setDisplayList(searchByButtons(dataManager.getDisplayList(),"1B")); 
        });
        
        secondBaseRadioButton.setOnAction(e -> { 
            dataManager.setDisplayList(searchByButtons(dataManager.getDisplayList(),"2B"));
        });
        thirdBaseRadioButton.setOnAction(e -> { 
            dataManager.setDisplayList(searchByButtons(dataManager.getDisplayList(),"3B"));
        });
        
        catcherRadioButton.setOnAction(e -> { 
            dataManager.setDisplayList(searchByButtons(dataManager.getDisplayList(),"C"));

        });
        
        shortStopRadioButton.setOnAction(e -> { 
            dataManager.setDisplayList(searchByButtons(dataManager.getDisplayList(),"SS"));

        });
        
        middleInfieldRadioButton.setOnAction(e -> { 
            dataManager.setDisplayList(searchByButtons(dataManager.getDisplayList(),"2B_SS")); 

        });
        
        cornerInfieldRadioButton.setOnAction(e -> { 
            dataManager.setDisplayList(searchByButtons(dataManager.getDisplayList(),"1B_3B"));

        });
        outfieldRadioButton.setOnAction(e -> { 
            dataManager.setDisplayList(searchByButtons(dataManager.getDisplayList(),"OF"));

        });
        
        pitcherRadioButton.setOnAction(e -> { 
            dataManager.setDisplayList(searchByButtons(dataManager.getDisplayList(),"P"));

        });
        
        //ADD/DELETE PLAYERS
        addPlayerButton.setOnAction(e-> {
            addPlayerDialog = new AddPlayerDialog(primaryStage,this); 
            addPlayerDialog.showDialog();
        });
        
        deletePlayerButton.setOnAction(e -> {
            try{
                dataManager.getPlayers().remove(getSelectedPlayer()); 
                dataManager.getDisplayList().remove(getSelectedPlayer()); 
            }catch(Exception w){
                w.printStackTrace();
            }
        });
        
        //TABLE DOUBLE CLICKING
        playersTable.setOnMouseClicked(e ->{
            if(e.getClickCount() == 2){
                //EDIT THE PLAYER 
                editPlayerDialog = new EditPlayerDialog(this,this.getSelectedPlayer(),primaryStage); 
                editPlayerDialog.showDialog();
            }     
        });
    }    
    private void searchByText(String text){
            //RESET THE BUTTONS
            for(RadioButton b : radioButtons){
                b.setSelected(false);
            }
            ArrayList<Player> newSearch = new ArrayList();  
            for(Player p : dataManager.getPlayers()){
                if(p.getLastName().contains(text)){
                    newSearch.add(p); 
                }
                else if(p.getFirstName().contains(text)){
                    newSearch.add(p); 
                }
                else if (p.getTeam().contains(text)){
                    newSearch.add(p); 
                }
            }
            dataManager.setDisplayList(FXCollections.observableList(newSearch));
            playersTable.setItems(FXCollections.observableList(dataManager.getDisplayList()));
    }
    private ObservableList<Player> searchByButtons(ObservableList<Player> list,String pos){ 
        ArrayList<Player> newList = new ArrayList(); 
        for(Player p: list){
            if(p.getQP().contains(pos)){
                newList.add(p);
            }
        }
        return FXCollections.observableList(newList); 
    }
    
    private void initWindow(String appTitle){
        wdkPane = new BorderPane(); 
        wdkPane.setTop(topToolbarPane);
        primaryScene = new Scene(wdkPane);
        centerPane = new BorderPane(); 
        centerPane.getStyleClass().add(CLASS_BORDERED_PANE); 
        wdkPane.setBottom(bottomToolbarPane);
        //SET THE STYLE SHEET
        primaryScene.getStylesheets().add(PRIMARY_STYLE_SHEET); 
        
        primaryStage.setScene(primaryScene); 
        primaryStage.setTitle(appTitle);
        // GET THE SIZE OF THE SCREEN
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();

        // AND USE IT TO SIZE THE WINDOW
        primaryStage.setX(bounds.getMinX());
        primaryStage.setY(bounds.getMinY());
        primaryStage.setWidth(bounds.getWidth());
        primaryStage.setHeight(bounds.getHeight());
        primaryStage.show(); 
    }
    
    // INIT A BUTTON AND ADD IT TO A CONTAINER IN A TOOLBAR
    private Button initChildButton(FlowPane toolbar, WDK_PropertyType icon, WDK_PropertyType tooltip, boolean disabled) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String imagePath = "file:" + PATH_IMAGES + props.getProperty(icon.toString());
        Image buttonImage = new Image(imagePath);
        Button button = new Button();
        button.setDisable(disabled);
        button.setGraphic(new ImageView(buttonImage));
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
        button.setTooltip(buttonTooltip);
        toolbar.getChildren().add(button); 
        return button;
    }
    
    //USE THIS FOR THE RADIO BUTTONS 
    private RadioButton initRadioButton(HBox pane, String text){
        RadioButton button = new RadioButton(text); 
        pane.getChildren().add(button);
        return button; 
    } 
    
    //USE LAMBA EXPRESSIONS TO INIT THE EVENT HANDLERS
    private void initEventHandlers(){
        fileController = new FileController(messageDialog, yesNoCancelDialog, draftFileManager);
        
        //TOP TOOL BAR BUTTON HANDLERS    
        newDraftButton.setOnAction(e -> {
           fileController.handleNewDraftRequest(this);
                   wdkPane.setCenter(centerPane);
        });
        saveDraftButton.setOnAction(e -> {
            fileController.handleSaveRequest(this); 
        });
        exitButton.setOnAction(e -> {
            fileController.handleExitRequest(this);});
        
        //BOTTOM TOOLBARHANDLERS FOR SCREEN SWITCHING 
        playersScreenButton.setOnAction(e -> { 
            centerPane.setCenter(playerPane); 
        });

        mlbTeamsScreenButton.setOnAction(e -> {
            centerPane.setCenter(mlbScreen);
        });

        fantasyTeamsScreenButton.setOnAction(e -> {
            centerPane.setCenter(new ScrollPane(fantasyTeamsScreen));
        });        

        fantasyStandingsScreenButton.setOnAction(e -> {
            centerPane.setCenter(fantasyStandingsScreen);
            fantasyStandingsScreen.getTeamTable().setItems(dataManager.getTeams());
        });             

        draftSummaryScreenButton.setOnAction(e -> {
            centerPane.setCenter(draftScreen);
        });   
    }
    
    
    //PUBLIC METHODS TO UPDATE THE UI 
    /**
     * This method is used to activate/deactivate tool bar buttons when
     * they can and cannot be used so as to provide foolproof design.
     * 
     * @param saved Describes whether the loaded Course has been saved or not.
     */
    public void updateToolbarControls(boolean saved) {
        // THIS TOGGLES WITH WHETHER THE CURRENT COURSE
        // HAS BEEN SAVED OR NOT
        saveDraftButton.setDisable(saved);

        // ALL THE OTHER BUTTONS ARE ALWAYS ENABLED
        // ONCE EDITING THAT FIRST COURSE BEGINS
        loadDraftButton.setDisable(false);
        exportDraftButton.setDisable(false);

        // NOTE THAT THE NEW, LOAD, AND EXIT BUTTONS
        // ARE NEVER DISABLED SO WE NEVER HAVE TO TOUCH THEM
        
        //ENABLE THE BOTTOM TOOLBAR
        playersScreenButton.setDisable(false); 
        fantasyTeamsScreenButton.setDisable(false); 
        fantasyStandingsScreenButton.setDisable(false); 
        draftSummaryScreenButton.setDisable(false); 
        mlbTeamsScreenButton.setDisable(false); 
    }
    
    public Player getSelectedPlayer(){
        return playersTable.getSelectionModel().getSelectedItem(); 
    }
    
    public int calcEstimatedVal(Player p){
        //Find the needed hitters and pitchers
        int neededHitters; 
        int neededPitchers; 
        int draftedHitters = 0; 
        int draftedPitchers = 0;
        
        for(FantasyTeam f : dataManager.getTeams()){
            draftedHitters += f.getDraftedHitters();
            draftedPitchers += f.getDraftedPitchers(); 
        }
        neededHitters = ((dataManager.getTeams().size()) * 14) - draftedHitters; 
        neededPitchers = ((dataManager.getTeams().size()) * 9) - draftedPitchers; 
        
        try{
            Hitter h = ((Hitter)p);
            return neededHitters; 
        }catch(Exception e){
        
        }
        
        try{
            Pitcher pt = ((Pitcher)p);
            return neededPitchers; 
        }catch(Exception e){
        
        }
        return 0; 
    }
}
