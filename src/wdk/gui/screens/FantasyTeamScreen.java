/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.gui.screens;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.util.Callback;
import properties_manager.PropertiesManager;
import wdk.WDK_PropertyType;
import static wdk.WDK_Startup_Constants.PATH_CSS;
import static wdk.WDK_Startup_Constants.PATH_IMAGES;
import wdk.data.DataManager;
import wdk.data.FantasyTeam;
import wdk.data.Hitter;
import wdk.data.Pitcher;
import wdk.data.Player;
import wdk.gui.MessageDialog;
import wdk.gui.WDK_GUI;
import wdk.gui.dialogs.AddFantasyTeamDialog;
import wdk.gui.dialogs.EditFantasyTeamDialog;
import wdk.gui.dialogs.EditPlayerDialog;

/**
 * Because the different screens rely on the the center pane
 * The Screens will have their own class extending BorderPane to organize stuff
 * @author Sanjay
 */
public class FantasyTeamScreen extends BorderPane{
    //GUI
    WDK_GUI gui; 
    DataManager data; 
    //CONTAINERS
    BorderPane topPane; 
    HBox draftBox;
    HBox teamBox; 
    BorderPane tablePane; 
    BorderPane taxiPane;
    
    //OUR COMPONENTS
    Label headingLabel; 
    Label draftNameLabel; 
    Label taxiSquadLabel; 
    TextField draftNameTextField; 
    Label selectTeamLabel; 
    Label startingLineupLabel; 
    
    //BUTTONS
    Button addTeamButton; 
    Button deleteTeamButton; 
    Button editNameButton; 
    Button freePlayerButton; 
    
    ComboBox teamsComboBox; 
    
    //TABLE CONTROLS 
    TableView<Player> startingLineup; 
    TableView<Player> taxiSquad; 
    
    //NOW THE COLUMNS 
    TableColumn positionColumn; 
    TableColumn lastNameColumn; 
    TableColumn firstNameColumn; 
    TableColumn proTeamColumn; 
    TableColumn qualifiedPositionsColumn;  
    TableColumn runsWinsColumn; 
    TableColumn homeRunsSavesColumn; 
    TableColumn rbiStrikeOutsColumn; 
    TableColumn sbERAColumn; 
    TableColumn baWHIPColumn; 
    TableColumn estValColumn; 
    TableColumn salaryColumn;
    TableColumn contractColumn;
    
    TableColumn taxiPositionColumn; 
    TableColumn taxiLastNameColumn; 
    TableColumn taxiFirstNameColumn; 
    TableColumn taxiProTeamColumn; 
    TableColumn taxiQualifiedPositionsColumn;  
    TableColumn taxiRunsWinsColumn; 
    TableColumn taxiHomeRunsSavesColumn; 
    TableColumn taxiRbiStrikeOutsColumn; 
    TableColumn taxiSbERAColumn; 
    TableColumn taxiBaWHIPColumn; 
    TableColumn taxiEstValColumn; 
    TableColumn taxiSalaryColumn;
    TableColumn taxiContractColumn;
    //DATA
    ObservableList<Player> playerList; 
    
    //CONSTANTS FOR STYLE
        //Constants used for settings
    static final String PRIMARY_STYLE_SHEET = PATH_CSS + "wdk_style.css";
    static final String CLASS_BORDERED_PANE = "bordered_pane"; 
    static final String CLASS_HEADING_LABEL = "heading_label"; 
    static final String CLASS_SUBHEADING_LABEL = "subheading_label"; 
    static final String CLASS_PROMPT_LABEL = "prompt_label"; 
    static final String CLASS_SCREEN_PANE = "screen_pane";
    static final int LARGE_TEXT_FIELD_LENGTH = 20;
    static final int SMALL_TEXT_FIELD_LENGTH = 5;
    
    //COLUMN HEADERS
    static final String COL_POS = "Position";
    static final String COL_LAST_NAME = "Last";
    static final String COL_FIRST_NAME = "First"; 
    static final String COL_PRO_TEAM = "Pro Team"; 
    static final String COL_QP = "Positions"; 
    static final String COL_RUNS_WINS = "R/W"; 
    static final String COL_HR_SV = "HR/SV"; 
    static final String COL_RBI_K = "RBI/K"; 
    static final String COL_SB_ERA = "SB/ERA"; 
    static final String COL_BA_WHIP = "BA/WHIP"; 
    static final String COL_ESTIMATED_VALUE = "Esitimated Vaule"; 
    static final String COL_SALARY = "Salary"; 
    static final String COL_CONTRACT = "Contract"; 
    
    //DIALOGS
    AddFantasyTeamDialog addTeamDialog; 
    EditPlayerDialog editPlayerDialog;
    MessageDialog messageDialog; 
    
    //POINTERS
    FantasyTeam curTeam; 
    ObservableList<FantasyTeam>  list; 
    
    public FantasyTeamScreen(WDK_GUI gui){
        this.gui = gui; 
        this.data = gui.getDataManager(); 
        messageDialog = new MessageDialog(gui.getPrimaryStage(),"ok"); 
        initControls(); 
        initTable(); 
        initTaxiTable(); 
        initStyle(); 
    }
    
    public DataManager getDataManager(){
        return this.data; 
    }

    public ObservableList<FantasyTeam> getList() {
        return list;
    }
    public ComboBox getTeamsComboBox(){
        return this.teamsComboBox; 
    }
    private void initControls(){
        topPane = new BorderPane(); 
        draftBox = new HBox();
        teamBox = new HBox(); 
        //LABELS
        headingLabel = new Label("Fantasy Teams"); 
        draftNameLabel = new Label("Draft Name:");
        selectTeamLabel = new Label("Select Team:"); 
        startingLineupLabel = new Label("Starting Lineup");
        //BUTTONS
        addTeamButton = initChildButton(teamBox,WDK_PropertyType.ADD_ICON,WDK_PropertyType.ADD_TEAM_TOOLTIP,false); 
        deleteTeamButton = initChildButton(teamBox,WDK_PropertyType.MINUS_ICON,WDK_PropertyType.DELETE_TEAM_TOOLTIP,false);
        editNameButton = new Button("EDIT"); 
        freePlayerButton = initChildButton(teamBox,WDK_PropertyType.MOVE_UP_ICON,WDK_PropertyType.FREE_PLAYER_TOOLTIP,false); 
        //TEXT FIELD
        draftNameTextField = new TextField(); 
        //NOW THE COMBO BOX
        teamsComboBox = new ComboBox();  
        list = data.getTeams(); 
        teamsComboBox.setItems(list);
        //ORGINIZE INTO CONTAINERS
        topPane.setTop(headingLabel);
        
        draftBox.getChildren().add(draftNameLabel);
        draftBox.getChildren().add(draftNameTextField);
        
        teamBox.getChildren().add(editNameButton); 
        teamBox.getChildren().add(selectTeamLabel);
        teamBox.getChildren().add(teamsComboBox); 

        topPane.setCenter(draftBox);
        topPane.setBottom(teamBox);
        this.setTop(topPane);
        
    }
    
    private void initStyle(){
        this.getStylesheets().add(PRIMARY_STYLE_SHEET); 
        this.getStyleClass().add(CLASS_SCREEN_PANE);
        headingLabel.getStyleClass().add(CLASS_HEADING_LABEL); 
        draftNameLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        selectTeamLabel.getStyleClass().add(CLASS_PROMPT_LABEL); 
        tablePane.getStyleClass().add(CLASS_BORDERED_PANE);
        taxiPane.getStyleClass().add(CLASS_BORDERED_PANE);
        startingLineupLabel.getStyleClass().add(CLASS_SUBHEADING_LABEL); 
        taxiSquadLabel.getStyleClass().add(CLASS_SUBHEADING_LABEL); 
    }
    
    private void initTable(){
        tablePane = new BorderPane(); 

        tablePane.setTop(startingLineupLabel); 
        startingLineup = new TableView(); 
        
        positionColumn = new TableColumn(COL_POS); 
        lastNameColumn = new TableColumn(COL_LAST_NAME); 
        firstNameColumn = new TableColumn(COL_FIRST_NAME); 
        proTeamColumn = new TableColumn(COL_PRO_TEAM); 
        qualifiedPositionsColumn = new TableColumn(COL_QP);
        runsWinsColumn = new TableColumn(COL_RUNS_WINS); 
        homeRunsSavesColumn = new TableColumn(COL_HR_SV); 
        rbiStrikeOutsColumn = new TableColumn(COL_RBI_K);
        sbERAColumn = new TableColumn(COL_SB_ERA); 
        baWHIPColumn = new TableColumn(COL_BA_WHIP); 
        estValColumn = new TableColumn(COL_ESTIMATED_VALUE); 
        salaryColumn = new TableColumn(COL_SALARY); 
        contractColumn = new TableColumn(COL_CONTRACT); 
        //ADD THE COLUMNS 
        startingLineup.getColumns().add(positionColumn);
        startingLineup.getColumns().add(lastNameColumn); 
        startingLineup.getColumns().add(firstNameColumn); 
        startingLineup.getColumns().add(proTeamColumn); 
        startingLineup.getColumns().add(qualifiedPositionsColumn);
        startingLineup.getColumns().add(runsWinsColumn);
        startingLineup.getColumns().add(homeRunsSavesColumn); 
        startingLineup.getColumns().add(rbiStrikeOutsColumn); 
        startingLineup.getColumns().add(sbERAColumn);
        startingLineup.getColumns().add(baWHIPColumn);
        startingLineup.getColumns().add(estValColumn);
        startingLineup.getColumns().add(contractColumn);
        startingLineup.getColumns().add(salaryColumn); 
        //BIND THE DATA
        positionColumn.setCellValueFactory(new PropertyValueFactory<String, String>("fQP"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("lastName"));
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("firstName"));
        proTeamColumn.setCellValueFactory(new PropertyValueFactory<String, String>("team"));
        qualifiedPositionsColumn.setCellValueFactory(new PropertyValueFactory<String, String>("QP"));
        salaryColumn.setCellValueFactory(new PropertyValueFactory<String, String>("salary"));
        contractColumn.setCellValueFactory(new PropertyValueFactory<String, String>("contract"));
        runsWinsColumn.setCellValueFactory(new Callback(){
            @Override 
            public ObservableValue call(Object param){
                TableColumn.CellDataFeatures x = (TableColumn.CellDataFeatures) param; 
                if(x.getValue().getClass().equals((new Hitter()).getClass())) return new SimpleIntegerProperty(((Hitter)x.getValue()).getR());
                else if(x.getValue().getClass().equals((new Pitcher()).getClass())) return new SimpleIntegerProperty(((Pitcher)x.getValue()).getW());
                else return new SimpleIntegerProperty(0);
                }});
         rbiStrikeOutsColumn.setCellValueFactory(new Callback(){
            @Override 
            public ObservableValue call(Object param){
                TableColumn.CellDataFeatures x = (TableColumn.CellDataFeatures) param; 
                if(x.getValue().getClass().equals((new Hitter()).getClass())) return new SimpleIntegerProperty(((Hitter)x.getValue()).getRBI());
                else if(x.getValue().getClass().equals((new Pitcher()).getClass())) return new SimpleIntegerProperty(((Pitcher)x.getValue()).getK());
                else return new SimpleIntegerProperty(0);
                }});
         sbERAColumn.setCellValueFactory(new Callback(){
            @Override 
            public ObservableValue call(Object param){
                TableColumn.CellDataFeatures x = (TableColumn.CellDataFeatures) param; 
                if(x.getValue().getClass().equals((new Hitter()).getClass())) return new SimpleIntegerProperty(((Hitter)x.getValue()).getSB());
                else if(x.getValue().getClass().equals((new Pitcher()).getClass())) return new SimpleDoubleProperty(((Pitcher)x.getValue()).getERA());
                else return new SimpleIntegerProperty(0);
                }});
         baWHIPColumn.setCellValueFactory(new Callback(){
            @Override 
            public ObservableValue call(Object param){
                TableColumn.CellDataFeatures x = (TableColumn.CellDataFeatures) param; 
                if(x.getValue().getClass().equals((new Hitter()).getClass())) return new SimpleDoubleProperty(((Hitter)x.getValue()).getBA());
                else if(x.getValue().getClass().equals((new Pitcher()).getClass())) return new SimpleDoubleProperty(((Pitcher)x.getValue()).getWHIP());
                else return new SimpleIntegerProperty(0);
                }});
         homeRunsSavesColumn.setCellValueFactory(new Callback(){
            @Override 
            public ObservableValue call(Object param){
                TableColumn.CellDataFeatures x = (TableColumn.CellDataFeatures) param; 
                if(x.getValue().getClass().equals((new Hitter()).getClass())) return new SimpleIntegerProperty(((Hitter)x.getValue()).getHR());
                else if(x.getValue().getClass().equals((new Pitcher()).getClass())) return new SimpleIntegerProperty(((Pitcher)x.getValue()).getSV());
                else return new SimpleIntegerProperty(0);
                }});
        
        //EVENT HANDLERS
        initHandlers(); 
        //ADD  THE TABLE
        tablePane.setCenter(startingLineup);
        this.setCenter(tablePane);
        
    }
    
    public TableView<Player> getTable(){
        return this.startingLineup; 
    }
    
    public ComboBox getComboBox(){
        return this.teamsComboBox; 
    }
    // INIT A BUTTON AND ADD IT TO A CONTAINER IN A TOOLBAR
    private Button initChildButton(HBox toolbar, WDK_PropertyType icon, WDK_PropertyType tooltip, boolean disabled) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String imagePath = "file:" + PATH_IMAGES + props.getProperty(icon.toString());
        Image buttonImage = new Image(imagePath);
        Button button = new Button();
        button.setDisable(disabled);
        button.setGraphic(new ImageView(buttonImage));
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
        button.setTooltip(buttonTooltip);
        toolbar.getChildren().add(button); 
        return button;
    }
    
    private void initHandlers(){
        addTeamButton.setOnAction(e -> {
            addTeamDialog = new AddFantasyTeamDialog(gui.getPrimaryStage(),this); 
            addTeamDialog.show();

        });
        
        deleteTeamButton.setOnAction(e -> {
            try{
                   curTeam = (FantasyTeam)teamsComboBox.getSelectionModel().getSelectedItem(); 
                //RETURN THE PLAYERS
                for(Player p: curTeam.getRoster()){
                    editPlayerDialog.freeAgent(p);
                }
            
                data.getTeams().remove(((FantasyTeam)teamsComboBox.getSelectionModel().getSelectedItem()).getName()); 
            }catch(Exception w){
                w.printStackTrace();
            }
        });
        
        teamsComboBox.setOnAction(e -> {

            this.startingLineup.setItems(((FantasyTeam)teamsComboBox.getSelectionModel().getSelectedItem()).getRoster()); 
            taxiSquad.setItems(((FantasyTeam)teamsComboBox.getSelectionModel().getSelectedItem()).getTaxiSquad()); 
        });
        
        draftNameTextField.setOnAction( e -> {
            gui.getDataManager().getDraft().setName(draftNameTextField.getText());
        });       
        
                //TABLE DOUBLE CLICKING
        startingLineup.setOnMouseClicked(e ->{
            if(e.getClickCount() == 2){
                //EDIT THE PLAYER 
                editPlayerDialog = new EditPlayerDialog(gui,startingLineup.getSelectionModel().getSelectedItem(),gui.getPrimaryStage()); 
                editPlayerDialog.showDialog();
            }     
        });
        
        freePlayerButton.setOnAction(e -> {
            try{
                freePlayer(startingLineup.getSelectionModel().getSelectedItem());
            }catch(Exception w){
                messageDialog.show("Please select a player");
            }
        });
        
        editNameButton.setOnAction(e -> {
            try{
                EditFantasyTeamDialog editFantasyTeamDialog = new EditFantasyTeamDialog(gui.getPrimaryStage(),this,(FantasyTeam)teamsComboBox.getSelectionModel().getSelectedItem());
                editFantasyTeamDialog.show();
            }catch(Exception w){
                messageDialog.show("Please Select a team"); 
            }
        });
    }     
    
    private void freePlayer(Player p){
        p.setDrafted(false);
        ((FantasyTeam)teamsComboBox.getSelectionModel().getSelectedItem()).getRoster().remove(p); 
        if(data.getDraftedPlayers().contains(p))data.getDraftedPlayers().remove(p); 
        data.getPlayers().add(p); 
    }
    
    private void initTaxiTable(){ 
        taxiPane = new BorderPane(); 
        taxiSquad = new TableView(); 
        
        taxiPositionColumn = new TableColumn(COL_POS); 
        taxiLastNameColumn = new TableColumn(COL_LAST_NAME); 
        taxiFirstNameColumn = new TableColumn(COL_FIRST_NAME); 
        taxiProTeamColumn = new TableColumn(COL_PRO_TEAM); 
        taxiQualifiedPositionsColumn = new TableColumn(COL_QP);
        taxiRunsWinsColumn = new TableColumn(COL_RUNS_WINS); 
        taxiHomeRunsSavesColumn = new TableColumn(COL_HR_SV); 
        taxiRbiStrikeOutsColumn = new TableColumn(COL_RBI_K);
        taxiSbERAColumn = new TableColumn(COL_SB_ERA); 
        taxiBaWHIPColumn = new TableColumn(COL_BA_WHIP); 
        taxiEstValColumn = new TableColumn(COL_ESTIMATED_VALUE); 
        taxiSalaryColumn = new TableColumn(COL_SALARY); 
        taxiContractColumn = new TableColumn(COL_CONTRACT); 
        //ADD THE COLUMNS 
        taxiSquad.getColumns().add(taxiPositionColumn);
        taxiSquad.getColumns().add(taxiLastNameColumn); 
        taxiSquad.getColumns().add(taxiFirstNameColumn); 
        taxiSquad.getColumns().add(taxiProTeamColumn); 
        taxiSquad.getColumns().add(taxiQualifiedPositionsColumn);
        taxiSquad.getColumns().add(taxiRunsWinsColumn);
        taxiSquad.getColumns().add(taxiHomeRunsSavesColumn); 
        taxiSquad.getColumns().add(taxiRbiStrikeOutsColumn); 
        taxiSquad.getColumns().add(taxiSbERAColumn);
        taxiSquad.getColumns().add(taxiBaWHIPColumn);
        taxiSquad.getColumns().add(taxiEstValColumn);
        taxiSquad.getColumns().add(taxiContractColumn);
        taxiSquad.getColumns().add(taxiSalaryColumn); 
        //BIND THE DATA
        taxiPositionColumn.setCellValueFactory(new PropertyValueFactory<String, String>("QP"));
        taxiLastNameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("lastName"));
        taxiFirstNameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("firstName"));
        taxiProTeamColumn.setCellValueFactory(new PropertyValueFactory<String, String>("team"));
        taxiQualifiedPositionsColumn.setCellValueFactory(new PropertyValueFactory<String, String>("QP"));
        taxiSalaryColumn.setCellValueFactory(new PropertyValueFactory<String, String>("salary"));
        taxiContractColumn.setCellValueFactory(new PropertyValueFactory<String, String>("contract"));
        taxiRunsWinsColumn.setCellValueFactory(new Callback(){
            @Override 
            public ObservableValue call(Object param){
                TableColumn.CellDataFeatures x = (TableColumn.CellDataFeatures) param; 
                if(x.getValue().getClass().equals((new Hitter()).getClass())) return new SimpleIntegerProperty(((Hitter)x.getValue()).getR());
                else if(x.getValue().getClass().equals((new Pitcher()).getClass())) return new SimpleIntegerProperty(((Pitcher)x.getValue()).getW());
                else return new SimpleIntegerProperty(0);
                }});
         taxiRbiStrikeOutsColumn.setCellValueFactory(new Callback(){
            @Override 
            public ObservableValue call(Object param){
                TableColumn.CellDataFeatures x = (TableColumn.CellDataFeatures) param; 
                if(x.getValue().getClass().equals((new Hitter()).getClass())) return new SimpleIntegerProperty(((Hitter)x.getValue()).getRBI());
                else if(x.getValue().getClass().equals((new Pitcher()).getClass())) return new SimpleIntegerProperty(((Pitcher)x.getValue()).getK());
                else return new SimpleIntegerProperty(0);
                }});
         taxiSbERAColumn.setCellValueFactory(new Callback(){
            @Override 
            public ObservableValue call(Object param){
                TableColumn.CellDataFeatures x = (TableColumn.CellDataFeatures) param; 
                if(x.getValue().getClass().equals((new Hitter()).getClass())) return new SimpleIntegerProperty(((Hitter)x.getValue()).getSB());
                else if(x.getValue().getClass().equals((new Pitcher()).getClass())) return new SimpleDoubleProperty(((Pitcher)x.getValue()).getERA());
                else return new SimpleIntegerProperty(0);
                }});
         taxiBaWHIPColumn.setCellValueFactory(new Callback(){
            @Override 
            public ObservableValue call(Object param){
                TableColumn.CellDataFeatures x = (TableColumn.CellDataFeatures) param; 
                if(x.getValue().getClass().equals((new Hitter()).getClass())) return new SimpleDoubleProperty(((Hitter)x.getValue()).getBA());
                else if(x.getValue().getClass().equals((new Pitcher()).getClass())) return new SimpleDoubleProperty(((Pitcher)x.getValue()).getWHIP());
                else return new SimpleIntegerProperty(0);
                }});
         taxiHomeRunsSavesColumn.setCellValueFactory(new Callback(){
            @Override 
            public ObservableValue call(Object param){
                TableColumn.CellDataFeatures x = (TableColumn.CellDataFeatures) param; 
                if(x.getValue().getClass().equals((new Hitter()).getClass())) return new SimpleIntegerProperty(((Hitter)x.getValue()).getHR());
                else if(x.getValue().getClass().equals((new Pitcher()).getClass())) return new SimpleIntegerProperty(((Pitcher)x.getValue()).getSV());
                else return new SimpleIntegerProperty(0);
                }});
        
        //EVENT HANDLERS
        initHandlers(); 
        //ADD  THE TABLE
        taxiSquadLabel = new Label("Taxi Squad"); 
        taxiPane.setTop(taxiSquadLabel); 
        taxiPane.setCenter(taxiSquad);
        //TABLE DOUBLE CLICKING
        startingLineup.setOnMouseClicked(e ->{
            if(e.getClickCount() == 2){
                //EDIT THE PLAYER 
                editPlayerDialog = new EditPlayerDialog(gui,startingLineup.getSelectionModel().getSelectedItem(),gui.getPrimaryStage()); 
                editPlayerDialog.showDialog();
            }     
        });
        this.setBottom(taxiPane);
        
    }
}
