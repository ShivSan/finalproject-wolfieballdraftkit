/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.gui.screens;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.util.Callback;
import wdk.data.DataManager;
import wdk.data.FantasyTeam;
import wdk.gui.dialogs.EditPlayerDialog;

/**
 *
 * @author Sanjay
 */
public class FantasyStandingsScreen extends BorderPane{
    Label headingLabel; 
    DataManager data; 
    TableView<FantasyTeam> teamTable; 
    TableColumn teamNameColumn; 
    TableColumn playersNeededColumn; 
    TableColumn payrollColumn; 
    TableColumn ppColumn; 
    TableColumn runsColumn; 
    TableColumn hrColumn; 
    TableColumn rbiColumn ; 
    TableColumn sbColumn; 
    TableColumn baColumn; 
    TableColumn wColumn; 
    TableColumn svColumn;
    TableColumn kColumn; 
    TableColumn eraColumn; 
    TableColumn whipColumn;
    TableColumn pointsColumn; 
    
    public FantasyStandingsScreen(DataManager data){
        this.data = data; 
        
        headingLabel = new Label("Fantasy Team Standings"); 
        teamTable = new TableView(); 
        
        teamNameColumn = new TableColumn("Team Name"); 
        playersNeededColumn = new TableColumn("Players Needed"); 
        payrollColumn = new TableColumn("$ left"); 
        ppColumn = new TableColumn("$ PP"); 
        runsColumn = new TableColumn("R");
        hrColumn = new TableColumn("HR"); 
        rbiColumn = new TableColumn("RBI");
        sbColumn = new TableColumn("SB"); 
        baColumn = new TableColumn("BA");
        wColumn = new TableColumn("W");
        svColumn = new TableColumn("SV");
        kColumn = new TableColumn("K");
        eraColumn = new TableColumn("ERA");
        whipColumn = new TableColumn("WHIP"); 
        pointsColumn = new TableColumn("Points");
        
        teamTable.getColumns().add(teamNameColumn); 
        teamTable.getColumns().add(playersNeededColumn); 
        teamTable.getColumns().add(payrollColumn); 
        teamTable.getColumns().add(ppColumn); 
        teamTable.getColumns().add(runsColumn);
        teamTable.getColumns().add(hrColumn);
        teamTable.getColumns().add(rbiColumn); 
        teamTable.getColumns().add(sbColumn);
        teamTable.getColumns().add(baColumn); 
        teamTable.getColumns().add(wColumn);
        teamTable.getColumns().add(svColumn);
        teamTable.getColumns().add(kColumn); 
        teamTable.getColumns().add(eraColumn); 
        teamTable.getColumns().add(whipColumn); 
        teamTable.getColumns().add(pointsColumn); 
        
        teamNameColumn.setCellValueFactory(new Callback(){
            @Override 
            public ObservableValue call(Object param){
                TableColumn.CellDataFeatures x = (TableColumn.CellDataFeatures) param; 
                FantasyTeam f = (FantasyTeam)x.getValue();
                    return new SimpleStringProperty(f.getName()); 
                }});
        
        playersNeededColumn.setCellValueFactory(new Callback(){
            @Override 
            public ObservableValue call(Object param){
                TableColumn.CellDataFeatures x = (TableColumn.CellDataFeatures) param; 
                FantasyTeam f = (FantasyTeam)x.getValue();
                    return new SimpleIntegerProperty(f.getRemainingSpots()); 
                }});
        payrollColumn.setCellValueFactory(new PropertyValueFactory<String, Integer>("payroll"));
        ppColumn.setCellValueFactory(new Callback(){
            @Override 
            public ObservableValue call(Object param){
                TableColumn.CellDataFeatures x = (TableColumn.CellDataFeatures) param; 
                FantasyTeam f = (FantasyTeam)x.getValue();
                    try{
                        return new SimpleIntegerProperty(f.getPayroll()/f.getRemainingSpots()); 
                    }catch(Exception e){
                        return new SimpleIntegerProperty(-1);
                    }
                }});
        
        runsColumn.setCellValueFactory(new Callback(){
            @Override 
            public ObservableValue call(Object param){
                TableColumn.CellDataFeatures x = (TableColumn.CellDataFeatures) param; 
                FantasyTeam f = (FantasyTeam)x.getValue();
                    return new SimpleIntegerProperty(f.getR()); 
                }});  
        hrColumn.setCellValueFactory(new PropertyValueFactory<String, Integer>("HR"));
        rbiColumn.setCellValueFactory(new PropertyValueFactory<String, Integer>("RBI"));
        sbColumn.setCellValueFactory(new PropertyValueFactory<String, Integer>("SB"));
        baColumn.setCellValueFactory(new PropertyValueFactory<String, Double>("BA"));
        wColumn.setCellValueFactory(new PropertyValueFactory<String, Integer>("W"));
        svColumn.setCellValueFactory(new PropertyValueFactory<String, Integer>("SV"));
        kColumn.setCellValueFactory(new PropertyValueFactory<String, Integer>("K"));
        eraColumn.setCellValueFactory(new PropertyValueFactory<String, Integer>("ERA"));
        whipColumn.setCellValueFactory(new PropertyValueFactory<String, Double>("WHIP"));
        teamTable.setItems(data.getTeams());
        
                //TABLE DOUBLE CLICKING
        teamTable.setOnMouseClicked(e ->{
            if(e.getClickCount() == 2){
            }     
        });
        this.setTop(headingLabel);
        this.setCenter(teamTable);
    }

    public TableView<FantasyTeam> getTeamTable() {
        return teamTable;
    }

    public void setTeamTable(TableView<FantasyTeam> teamTable) {
        this.teamTable = teamTable;
    }
    
    public void assignPoints(){
        ObservableList<Integer> intList; 
        ObservableList<Double> doubleList; 
        
        //START WITH HITS 
        for(FantasyTeam f: data.getTeams()){
            
        }
    }
    
}
