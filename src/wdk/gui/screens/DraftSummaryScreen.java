/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.gui.screens;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.util.Callback;
import properties_manager.PropertiesManager;
import wdk.WDK_PropertyType;
import static wdk.WDK_Startup_Constants.PATH_CSS;
import static wdk.WDK_Startup_Constants.PATH_IMAGES;
import wdk.data.DataManager;
import wdk.data.FantasyTeam;
import wdk.data.Player;
import wdk.gui.WDK_GUI;

/**
 * This screen displays the draft order and implements the automated draft 
 * @author Sanjay
 */
public class DraftSummaryScreen extends BorderPane{
    //OUR DATA 
    DataManager data; 
    WDK_GUI gui; 
    //Labels
    Label headingLabel; 
    //Buttons
    FlowPane buttonPane; 
    Button bestPlayerButton; 
    Button startDraftButton; 
    Button pauseDraftButton; 
    
    //TABLE 
    TableView<Player> draftTable;
    TableColumn pickColumn;
    TableColumn lastNameColumn;
    TableColumn firstNameColumn;
    TableColumn fantasyTeamColumn; 
    TableColumn contractColumn; 
    TableColumn salaryColumn; 
    //Constants used for settings
    static final String PRIMARY_STYLE_SHEET = PATH_CSS + "wdk_style.css";
    static final String CLASS_BORDERED_PANE = "bordered_pane"; 
    static final String CLASS_HEADING_LABEL = "heading_label"; 
    static final String CLASS_SUBHEADING_LABEL = "subheading_label"; 
    static final String CLASS_PROMPT_LABEL = "prompt_label"; 
    static final String CLASS_SCREEN_PANE = "screen_pane";
    static final int LARGE_TEXT_FIELD_LENGTH = 20;
    static final int SMALL_TEXT_FIELD_LENGTH = 5;
    
    
    public DraftSummaryScreen(WDK_GUI gui){
        this.gui = gui;
        this.data = gui.getDataManager(); 
        //SET THE STYLE SHEET
        this.getStylesheets().add(PRIMARY_STYLE_SHEET); 
        
        headingLabel = new Label("Draft Summary");
        buttonPane = new FlowPane();
        bestPlayerButton = initChildButton(buttonPane,WDK_PropertyType.ADD_ICON,WDK_PropertyType.ADD_RANDOM_PLAYER_TOOLTIP,false); 
        startDraftButton = initChildButton(buttonPane,WDK_PropertyType.PLAY_BUTTON_ICON,WDK_PropertyType.START_DRAFT_TOOLTIP,false);
        pauseDraftButton = initChildButton(buttonPane,WDK_PropertyType.PAUSE_BUTTON_ICON,WDK_PropertyType.PAUSE_DRAFT_TOOLTIP,false);
        
        initTable(); 
        draftTable.setItems(data.getDraftedPlayers()); 
        initHandlers();
        this.getStyleClass().add(CLASS_SCREEN_PANE);
        headingLabel.getStyleClass().add(CLASS_HEADING_LABEL);
        this.setTop(headingLabel);
        this.setCenter(buttonPane);
        this.setBottom(draftTable);
    }
    
    private void initTable(){
        draftTable = new TableView();
        pickColumn = new TableColumn("Pick"); 
        lastNameColumn = new TableColumn("Last");
        firstNameColumn = new TableColumn("First"); 
        fantasyTeamColumn = new TableColumn("Fantasy Team"); 
        contractColumn = new TableColumn("Contract");
        salaryColumn = new TableColumn("Salary");
        
        draftTable.getColumns().add(pickColumn);
        draftTable.getColumns().add(lastNameColumn);
        draftTable.getColumns().add(firstNameColumn);
        draftTable.getColumns().add(fantasyTeamColumn);
        draftTable.getColumns().add(contractColumn);
        draftTable.getColumns().add(salaryColumn); 
        
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("lastName"));
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("firstName"));
        fantasyTeamColumn.setCellValueFactory(new Callback(){
            @Override 
            public ObservableValue call(Object param){
                TableColumn.CellDataFeatures x = (TableColumn.CellDataFeatures) param; 
                Player p = (Player)x.getValue();
                    return new SimpleStringProperty(p.getfTeam().getName()); 
                }});
        salaryColumn.setCellValueFactory(new PropertyValueFactory<String, String>("salary"));
        contractColumn.setCellValueFactory(new PropertyValueFactory<String, String>("contract"));
        
        pickColumn.setCellValueFactory(new Callback<CellDataFeatures<Player, String>, ObservableValue<String>>() {
            @Override public ObservableValue<String> call(CellDataFeatures<Player, String> p) {
            return new ReadOnlyObjectWrapper(draftTable.getItems().indexOf(p.getValue()) + 1 + "");
                }   
            });   
        pickColumn.setSortable(false);
    }
    
    private void initHandlers(){
        Task  task = autoPick();
        Thread thread = new Thread(task);
        
        bestPlayerButton.setOnAction(e -> {
            int i = 0; 
            boolean allFull = false; 
            FantasyTeam f = data.getTeams().get(i); 
            try{
                while(f.isFull()){
                    f = data.getTeams().get(i++);
                    allFull = f.isFull(); 
                }
            }catch(Exception d){
                
                }
            if(allFull){getTaxiPlayer(f);}
            else this.getRandomPlayer(f);
        });
        
        startDraftButton.setOnAction(e ->{
            if(thread.isAlive()){
                thread.resume();
            }
            else thread.start(); 
        });
        
        pauseDraftButton.setOnAction(e ->{
            thread.suspend();
        });
    }
    
    private Task autoPick(){
        Task<Void> task = new Task<Void>() {
        ObservableList<Player> list = FXCollections.observableList(data.getPlayers()); 
         @Override
        protected Void call() throws Exception{
               int x = 0; 
               while(x < data.getTeams().size() * 5){
               for(FantasyTeam f: data.getTeams() ){
                   if(f.isFull()){
                       //draft taxiPlayers
                       getTaxiPlayer(f);
                       Thread.sleep(1000);
                       x++; 
                   }
                   else{
                   //2 CATCHERS
                   draftRandomPlayer(f,"C"); 
                   Thread.sleep(1000); 
                   draftRandomPlayer(f,"C");
                   Thread.sleep(1000); 
                   //1 1B
                   draftRandomPlayer(f,"1B"); 
                   Thread.sleep(1000);
                   //1 CI
                   draftRandomPlayer(f,"CI"); 
                   Thread.sleep(1000);
                   //1 3B 
                   draftRandomPlayer(f,"3B"); 
                   Thread.sleep(1000);  
                   //1 2B
                   draftRandomPlayer(f,"2B"); 
                   Thread.sleep(1000);  
                   //1MI
                   draftRandomPlayer(f,"MI"); 
                   Thread.sleep(1000); 
                   //1 SS
                   draftRandomPlayer(f,"SS"); 
                   Thread.sleep(1000);  
                   //5 OF
                   for(int i = 0; i < 5; i ++){
                       draftRandomPlayer(f,"OF"); 
                       Thread.sleep(1000);
                   }
                   //1 U
                   draftRandomPlayer(f,"U"); 
                   Thread.sleep(1000);
                   //9 P
                   for(int j = 0; j < 9; j ++){
                       draftRandomPlayer(f,"P"); 
                       Thread.sleep(1000);
                   }
                   }
                   
               }
                
               }
               return null;
           }
        };  
        return task; 
    }
    
    private boolean draftRandomPlayer(FantasyTeam f,String pos){
        ObservableList<Player> sortedList; 
        int index; 
            //IF CATCHERS
            if(pos.equals("C")){
                sortedList = sortPlayerList(pos); 
                Collections.shuffle(sortedList);
                index = 0; 
                //CHECK THE SPOTS 
                if(f.getRosterSpots().get(pos) < 2){
                    //ADD PLAYERS
                    Player p = sortedList.get(index); 
                    data.getDraftedPlayers().add(p);
                    data.getDisplayList().remove(p);
                    data.getPlayers().remove(p); 
                    p.setDrafted(true);
                    p.setFantasyTeam(f);
                    p.setFQP(pos);
                    p.setSalary(1);
                    p.setContract("S2"); 
                    return f.addPlayer(p, pos);
                }
            }
            
            //FIRST BASE
            else if(pos.equals("1B")){
                sortedList = sortPlayerList(pos); 
                Collections.shuffle(sortedList);
                index = 0;
                //CHECK THE SPOTS 
                if(f.getRosterSpots().get(pos) < 1){
                    //ADD PLAYERS
                    Player p = sortedList.get(index); 
                    data.getDraftedPlayers().add(p);
                    data.getDisplayList().remove(p);
                    data.getPlayers().remove(p); 
                    p.setDrafted(true);
                    p.setFantasyTeam(f);
                    p.setFQP(pos);
                    p.setSalary(1);
                    p.setContract("S2"); 
                    return f.addPlayer(p, pos);
                }
            }
            
            //CORNER INFIELD
            else if(pos.equals("CI")){
                sortedList = FXCollections.observableList(new ArrayList()); 
                for(Player pl : data.getPlayers()){
                    if(pl.getQP().contains("1B")||pl.getQP().contains("3B")){
                        sortedList.add(pl); 
                    }
                }
                Collections.shuffle(sortedList);
                index = 0;
                //CHECK THE SPOTS 
                if(f.getRosterSpots().get(pos) < 1){
                    //ADD PLAYERS
                    Player p = sortedList.get(index); 
                    data.getDraftedPlayers().add(p);
                    data.getDisplayList().remove(p);
                    data.getPlayers().remove(p); 
                    p.setDrafted(true);
                    p.setFantasyTeam(f);
                    p.setFQP(pos);
                    p.setSalary(1);
                    p.setContract("S2"); 
                    return f.addPlayer(p, pos);
                }
            }
            
            //THIRDBASE
            else if(pos.equals("3B")){
                sortedList = sortPlayerList(pos); 
                Collections.shuffle(sortedList);
                index = 0;
                //CHECK THE SPOTS 
                if(f.getRosterSpots().get(pos) < 1){
                    //ADD PLAYERS
                    Player p = sortedList.get(index); 
                    data.getDraftedPlayers().add(p);
                    data.getDisplayList().remove(p);
                    data.getPlayers().remove(p); 
                    p.setDrafted(true);
                    p.setFantasyTeam(f);
                    p.setFQP(pos);
                    p.setSalary(1);
                    p.setContract("S2"); 
                    return f.addPlayer(p, pos);
                }
            }
            
            //2B
            else if(pos.equals("2B")){
                sortedList = sortPlayerList(pos); 
                Collections.shuffle(sortedList);
                index = 0;
                //CHECK THE SPOTS 
                if(f.getRosterSpots().get(pos) < 1){
                    //ADD PLAYERS
                    Player p = sortedList.get(index); 
                    data.getDraftedPlayers().add(p);
                    data.getDisplayList().remove(p);
                    data.getPlayers().remove(p); 
                    p.setDrafted(true);
                    p.setFantasyTeam(f);
                    p.setFQP(pos);
                    p.setSalary(1);
                    p.setContract("S2"); 
                    return f.addPlayer(p, pos);
                }
            }
            
            //SHORTSTOP 
            else if(pos.equals("SS")){
                sortedList = sortPlayerList(pos); 
                Collections.shuffle(sortedList);
                index = 0;
                //CHECK THE SPOTS 
                if(f.getRosterSpots().get(pos) < 1){
                    //ADD PLAYERS
                    Player p = sortedList.get(index); 
                    data.getDraftedPlayers().add(p);
                    data.getDisplayList().remove(p);
                    data.getPlayers().remove(p); 
                    p.setDrafted(true);
                    p.setFantasyTeam(f);
                    p.setFQP(pos);
                    p.setSalary(1);
                    p.setContract("S2");
                    return f.addPlayer(p, pos);
                }
            }
            
           //MIDDLE INFIELD
            else if(pos.equals("MI")){
                sortedList = FXCollections.observableList(new ArrayList()); 
                for(Player pl : data.getPlayers()){
                    if(pl.getQP().contains("2B")||pl.getQP().contains("SS")){
                        sortedList.add(pl); 
                    }
                }
                Collections.shuffle(sortedList);
                index = 0;
                //CHECK THE SPOTS 
                if(f.getRosterSpots().get(pos) < 1){
                    //ADD PLAYERS
                    Player p = sortedList.get(index); 
                    data.getDraftedPlayers().add(p);
                    data.getDisplayList().remove(p);
                    data.getPlayers().remove(p); 
                    p.setDrafted(true);
                    p.setFantasyTeam(f);
                    p.setFQP(pos);
                    p.setSalary(1);
                    p.setContract("S2"); 
                    return f.addPlayer(p, pos);
                }
            }
            
            //PICTHERS
            else if(pos.equals("P")){
                sortedList = sortPlayerList(pos); 
                Collections.shuffle(sortedList);
                index = 0;
                //CHECK THE SPOTS 
                if(f.getRosterSpots().get(pos) < 9){
                    //ADD PLAYERS
                    Player p = sortedList.get(index); 
                    data.getDraftedPlayers().add(p);
                    data.getDisplayList().remove(p);
                    data.getPlayers().remove(p); 
                    p.setDrafted(true);
                    p.setFantasyTeam(f);
                    p.setFQP(pos);
                    p.setContract("S2");
                    p.setSalary(1);
                    return f.addPlayer(p, pos);
                }
            }
            
            //OF 
            else if(pos.equals("OF")){
                sortedList = sortPlayerList(pos); 
                Collections.shuffle(sortedList);
                index = 0;
                //CHECK THE SPOTS 
                if(f.getRosterSpots().get(pos) < 5){
                    //ADD PLAYERS
                    Player p = sortedList.get(index); 
                    data.getDraftedPlayers().add(p);
                    data.getDisplayList().remove(p);
                    data.getPlayers().remove(p); 
                    p.setDrafted(true);
                    p.setFantasyTeam(f);
                    p.setFQP(pos);
                    p.setSalary(1);
                    p.setContract("S2"); 
                    return f.addPlayer(p, pos);
                }
            }
            
            //U
            else if(pos.equals("U")){
                sortedList = FXCollections.observableList(new ArrayList()); 
                for(Player pl : data.getPlayers()){
                    if(pl.getQP().contains("1B")||pl.getQP().contains("3B")
                            ||pl.getQP().contains("2B")||pl.getQP().contains("OF")
                            ||pl.getQP().contains("SS")){
                        sortedList.add(pl); 
                    }
                }
                Collections.shuffle(sortedList);
                index = 0;
                //CHECK THE SPOTS 
                if(f.getRosterSpots().get(pos) < 1){
                    //ADD PLAYERS
                    Player p = sortedList.get(index); 
                    data.getDraftedPlayers().add(p);
                    data.getDisplayList().remove(p);
                    data.getPlayers().remove(p); 
                    p.setDrafted(true);
                    p.setFantasyTeam(f);
                    p.setFQP(pos);
                    p.setSalary(1);
                    p.setContract("S2"); 
                    return f.addPlayer(p, pos);
                }
            }
            return false; 
        }
    
    private ObservableList<Player> sortPlayerList(String pos){
        ObservableList<Player> list = FXCollections.observableList(new ArrayList()); 
        for(Player p: data.getPlayers()){
            if(p.getQP().contains(pos)){
               list.add(p); 
            }
        }
        return list; 
    }
    
    public void getRandomPlayer(FantasyTeam f){
        if(draftRandomPlayer(f,"C")){
            return; 
        }
        else if (draftRandomPlayer(f,"1B")){
            return;
        }
        else if (draftRandomPlayer(f,"CI")){
            return;
        }
        else if (draftRandomPlayer(f,"3B")){
            return;
        }
        else if (draftRandomPlayer(f,"2B")){
            return;
        }
        else if (draftRandomPlayer(f,"MI")){
            return;
        }
        else if (draftRandomPlayer(f,"SS")){
            return;
        }
        else if (draftRandomPlayer(f,"OF")){
            return;
        }
        else if (draftRandomPlayer(f,"P")){
            return;
        }
        else if (draftRandomPlayer(f,"U")){
            return;
        }
    }
    
    public void getTaxiPlayer(FantasyTeam f){
        ObservableList<Player> list = FXCollections.observableList(data.getPlayers()); 
        Collections.shuffle(list);
        
        Player p = list.get(0); 
        
        f.getTaxiSquad().add(p); 
        data.getDisplayList().remove(p); 
        data.getPlayers().remove(p);
        data.getDraftedPlayers().add(p);
        p.setDrafted(true);
        p.setContract("X");
        p.setFantasyTeam(f); 
    }
    
    private Button initChildButton(FlowPane toolbar, WDK_PropertyType icon, WDK_PropertyType tooltip, boolean disabled) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String imagePath = "file:" + PATH_IMAGES + props.getProperty(icon.toString());
        Image buttonImage = new Image(imagePath);
        Button button = new Button();
        button.setDisable(disabled);
        button.setGraphic(new ImageView(buttonImage));
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
        button.setTooltip(buttonTooltip);
        toolbar.getChildren().add(button); 
        return button;
    }
}
