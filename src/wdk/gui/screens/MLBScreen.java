/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.gui.screens;

import java.util.ArrayList;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.util.Callback;
import static wdk.WDK_Startup_Constants.PATH_CSS;
import wdk.data.DataManager;
import wdk.data.Hitter;
import wdk.data.Pitcher;
import wdk.data.Player;

/**
 * You can view players by their team on this screen 
 * @author Sanjay
 */
public class MLBScreen extends BorderPane{
    Label headingLabel; 
    Label selectTeamLabel; 
    ComboBox teamsComboBox; 
    
    //OUT ACESS TO THE DATA
    DataManager data; 
    
    //CONTAINER FOR THE CONTROLS
    FlowPane teamsPane; 
    
    //TABLE
    TableView <Player> teamView;
    
    //COLUMNS
    TableColumn lastNameColumn; 
    TableColumn firstNameColumn; 
    TableColumn proTeamColumn; 
    TableColumn qualifiedPositionsColumn; 
    
    Button test; 
    
    //Constants used for settings
    static final String PRIMARY_STYLE_SHEET = PATH_CSS + "wdk_style.css";
    static final String CLASS_BORDERED_PANE = "bordered_pane"; 
    static final String CLASS_HEADING_LABEL = "heading_label"; 
    static final String CLASS_SUBHEADING_LABEL = "subheading_label"; 
    static final String CLASS_PROMPT_LABEL = "prompt_label"; 
    static final String CLASS_SCREEN_PANE = "screen_pane";
    static final int LARGE_TEXT_FIELD_LENGTH = 20;
    static final int SMALL_TEXT_FIELD_LENGTH = 5;
    
    
    //COLUMN CONTANTS    static final String COL_LAST_NAME = "Last Name";
    static final String COL_LAST_NAME = "Last Name"; 
    static final String COL_FIRST_NAME = "First Name"; 
    static final String COL_PRO_TEAM = "Pro Team"; 
    static final String COL_QP = "Positions"; 
    static final String COL_YEAR_OF_BIRTH = "Year Od Birth"; 
    static final String COL_RUNS = "R"; 
    static final String COL_HR = "HR"; 
    static final String COL_RBI = "RBI"; 
    static final String COL_SB = "SB"; 
    static final String COL_BA = "BA"; 
    static final String COL_ESTIMATED_VALUE = "Esitimated Vaule"; 
    static final String COL_NOTES = "Notes"; 
    
    public MLBScreen(DataManager data){
        this.data = data; 
        teamsPane = new FlowPane(); 
        headingLabel = new Label("MLB Teams");
        selectTeamLabel = new Label("Select team: ");
        teamsComboBox = new ComboBox();
        teamsComboBox.setItems(data.getMlbTeams()); 
        initStyle();
        this.setTop(headingLabel);
        teamsPane.getChildren().add(selectTeamLabel); 
        teamsPane.getChildren().add(teamsComboBox);
        this.setCenter(teamsPane);
        initTable(); 
        teamsPane.getChildren().add(teamView); 
        //allow comboBox Team Switching 
        teamsComboBox.setOnAction(e -> {
            
            teamView.setItems(getTeam((String)teamsComboBox.getSelectionModel().getSelectedItem()));
        });
    }
    public void tryIt(){
        Task<Void> task = new Task<Void>() {
            ObservableList<Player> list = FXCollections.observableList(new ArrayList()); 
            
            @Override
            protected Void call() throws Exception{
                for(Player p : getTeam("LAD")){
                    teamView.setItems(list); 
                    list.add(p);
                    Thread.sleep(1000);
                }
                return null; 
            }
        };  
        
        Thread thread = new Thread(task);
        thread.start();
    }
    public void initStyle(){
        this.getStylesheets().add(PRIMARY_STYLE_SHEET); 
        this.getStyleClass().add(CLASS_SCREEN_PANE); 
        headingLabel.getStyleClass().add(CLASS_HEADING_LABEL);
        selectTeamLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
    }
    public ObservableList<Player> getTeam(String team){
        ObservableList<Player> list = FXCollections.observableList(new ArrayList()); 
        for(Player p: data.getPlayers()){
            if(p.getTeam().contains(team)){
                list.add(p); 
            }
        }
        return list; 
    }
    public void initTable(){
         teamView = new TableView(); 
         lastNameColumn = new TableColumn(COL_LAST_NAME); 
         firstNameColumn = new TableColumn(COL_FIRST_NAME);
         proTeamColumn = new TableColumn(COL_PRO_TEAM); 
         qualifiedPositionsColumn = new TableColumn(COL_QP);
         
         teamView.getColumns().add(lastNameColumn); 
         teamView.getColumns().add(firstNameColumn); 
         teamView.getColumns().add(proTeamColumn);
         teamView.getColumns().add(qualifiedPositionsColumn); 
         
         //LINK THE COULUMNS TO THE DATA
         lastNameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("lastName"));
         firstNameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("firstName"));
         proTeamColumn.setCellValueFactory(new PropertyValueFactory<String, String>("team"));
         qualifiedPositionsColumn.setCellValueFactory(new PropertyValueFactory<String, String>("QP"));

    }
}
